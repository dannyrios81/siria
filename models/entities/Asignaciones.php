<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "asignaciones".
 *
 * @property int $id
 * @property int $idcompany
 * @property int $nit
 * @property string $name
 * @property int $idusers
 * @property string $categoria
 * @property string $archivo
 * @property string $fecha_cargue
 * @property string $estado
 * @property int $asignado
 *
 * @property Company $company
 * @property Users $users
 */
class Asignaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asignaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcompany', 'nit', 'name', 'categoria', 'archivo', 'fecha_cargue', 'estado', 'asignado'], 'required'],
            [['idcompany', 'nit', 'idusers', 'asignado'], 'integer'],
            [['fecha_cargue'], 'safe'],
            [['name', 'categoria', 'archivo', 'estado'], 'string', 'max' => 255],
            [['idcompany'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['idcompany' => 'idcompany']],
            [['idusers'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['idusers' => 'idusers']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcompany' => 'Idcompany',
            'nit' => 'Nit',
            'name' => 'Name',
            'idusers' => 'Idusers',
            'categoria' => 'Categoria',
            'archivo' => 'Archivo',
            'fecha_cargue' => 'Fecha Cargue',
            'estado' => 'Estado',
            'asignado' => 'Asignado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'idusers']);
    }
}
