<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "user_company".
 *
 * @property int $id_user
 * @property int $id_company
 * @property int $Estado_usuario
 * @property string $Random
 * @property string $iniciocargue
 * @property string $fincargue
 *
 * @property Users $user
 * @property Company $company
 */
class UserCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_company'], 'required'],
            [['id_user', 'id_company', 'Estado_usuario'], 'integer'],
            [['iniciocargue', 'fincargue'], 'safe'],
            [['Random'], 'string', 'max' => 11],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'idusers']],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'idcompany']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'id_company' => 'Id Company',
            'Estado_usuario' => 'Estado Usuario',
            'Random' => 'Random',
            'iniciocargue' => 'Iniciocargue',
            'fincargue' => 'Fincargue',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'id_company']);
    }
}
