<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "adjuntos".
 *
 * @property int $id
 * @property int $nombre
 * @property string $extension
 * @property int $id_company
 * @property int $id_adjunto
 * @property int $id_user
 * @property string $path
 *
 * @property Company $company
 * @property Tipoadjunto $adjunto
 * @property Users $user
 */
class Adjuntos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adjuntos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'extension', 'id_company', 'id_adjunto', 'id_user', 'path'], 'required'],
            [['nombre', 'id_company', 'id_adjunto', 'id_user'], 'integer'],
            [['extension'], 'string', 'max' => 4],
            [['path'], 'string', 'max' => 255],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'idcompany']],
            [['id_adjunto'], 'exist', 'skipOnError' => true, 'targetClass' => Tipoadjunto::className(), 'targetAttribute' => ['id_adjunto' => 'id_adjunto']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'idusers']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'extension' => 'Extension',
            'id_company' => 'Id Company',
            'id_adjunto' => 'Id Adjunto',
            'id_user' => 'Id User',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdjunto()
    {
        return $this->hasOne(Tipoadjunto::className(), ['id_adjunto' => 'id_adjunto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'id_user']);
    }
}
