<?php

namespace app\models\entities;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $idusers
 * @property string $name
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $authkey
 * @property string $token
 *
 * @property BioinsumosExportacion[] $bioinsumosExportacions
 * @property FertilizantesProduccion[] $fertilizantesProduccions
 * @property Authassignment[] $authassignments
 * @property Authitems[] $itemNames
 * @property UserCompany $userCompany
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'lastname', 'username', 'password'], 'string', 'max' => 100],
            [['authkey', 'token'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusers' => 'Idusers',
            'name' => 'Name',
            'lastname' => 'Lastname',
            'username' => 'Username',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getAuthassignments()
    {
        return $this->hasMany(Authassignment::className(), ['user_id' => 'idusers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(Authitems::className(), ['name' => 'item_name'])->viaTable('authassignment', ['user_id' => 'idusers']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBioinsumosExportacions()
    {
        return $this->hasMany(BioinsumosExportacion::className(), ['idusers' => 'idusers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFertilizantesProduccions()
    {
        return $this->hasMany(FertilizantesProduccion::className(), ['idusers' => 'idusers']);
    }

    public function getUserCompany()
    {
        return $this->hasOne(UserCompany::className(), ['id_user' => 'idusers']);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return Users::findOne(['idusers' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface|null the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->idusers;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled. The returned key will be stored on the
     * client side as a cookie and will be used to authenticate user even if PHP session has been expired.
     *
     * Make sure to invalidate earlier issued authKeys when you implement force user logout, password change and
     * other scenarios, that require forceful access revocation for old sessions.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
        return '';
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
        return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->select(['idusers','name','lastname','username','password','authkey','token'])
            ->where(['OR',['username'=>$username]])
            ->one();

    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password,$this->password);
    }
}
