<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $idcompany
 * @property string $name
 * @property int $nit
 * @property int $phone
 * @property string $address
 * @property string $email
 *
 * @property BioinsumosExportacion[] $bioinsumosExportacions
 * @property FertilizantesProduccion[] $fertilizantesProduccions
 * @property UserCompany[] $userCompanies
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'nit', 'phone', 'address', 'email'], 'required'],
            [['nit', 'phone'], 'integer'],
            [['name', 'address', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcompany' => 'Idcompany',
            'name' => 'Nombre',
            'nit' => 'Numero De Identtifición',
            'phone' => 'Telefono',
            'address' => 'Dirección',
            'email' => 'E-mail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBioinsumosExportacions()
    {
        return $this->hasMany(BioinsumosExportacion::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFertilizantesProduccions()
    {
        return $this->hasMany(FertilizantesProduccion::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['id_company' => 'idcompany']);
    }
}
