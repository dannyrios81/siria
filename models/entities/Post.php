<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Post".
 *
 * @property int $id
 * @property string $title
 * @property string $Body
 * @property string $created_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Body'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'Body' => 'Body',
            'created_at' => 'Created At',
        ];
    }
}
