<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "tipoadjunto".
 *
 * @property int $id_adjunto
 * @property string $nombreadjunto
 *
 * @property Adjuntos[] $adjuntos
 */
class Tipoadjunto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipoadjunto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreadjunto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_adjunto' => 'Id Adjunto',
            'nombreadjunto' => 'Nombreadjunto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdjuntos()
    {
        return $this->hasMany(Adjuntos::className(), ['id_adjunto' => 'id_adjunto']);
    }
}
