<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "app_skeleton_has_authitems".
 *
 * @property int $idapp_skeleton
 * @property string $auth_name
 *
 * @property Authitems $authName
 * @property AppSkeleton $appSkeleton
 */
class AppSkeletonHasAuthitems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_skeleton_has_authitems';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idapp_skeleton', 'auth_name'], 'required'],
            [['idapp_skeleton'], 'integer'],
            [['auth_name'], 'string', 'max' => 64],
            [['idapp_skeleton', 'auth_name'], 'unique', 'targetAttribute' => ['idapp_skeleton', 'auth_name']],
            [['auth_name'], 'exist', 'skipOnError' => true, 'targetClass' => Authitems::className(), 'targetAttribute' => ['auth_name' => 'name']],
            [['idapp_skeleton'], 'exist', 'skipOnError' => true, 'targetClass' => AppSkeleton::className(), 'targetAttribute' => ['idapp_skeleton' => 'idapp_skeleton']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idapp_skeleton' => 'Idapp Skeleton',
            'auth_name' => 'Auth Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthName()
    {
        return $this->hasOne(Authitems::className(), ['name' => 'auth_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeleton()
    {
        return $this->hasOne(AppSkeleton::className(), ['idapp_skeleton' => 'idapp_skeleton']);
    }
}
