<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Fechas".
 *
 * @property int $id
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $CreatedAt
 * @property int $nit
 * @property string $razon
 */
class Fechas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Fechas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin', 'CreatedAt'], 'safe'],
            [['nit'], 'integer'],
            [['razon'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'CreatedAt' => 'Created At',
            'nit' => 'nit',
            'razon' => 'Razon',
        ];
    }
}
