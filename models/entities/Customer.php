<?php

namespace app\models\entities;

use Yii;
use yii \ validators \ UniqueValidator;

/**
 * This is the model class for table "users".
 *
 * @property int $idusers
 * @property string $name
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $authkey
 * @property string $token
 *
 * @property Authassignment[] $authassignments
 * @property Authitems[] $itemNames
 * @property UserCompany[] $userCompanies
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'lastname', 'username', 'password'], 'string', 'max' => 100],
            [['authkey', 'token'], 'string', 'max' => 255],
            [['username'], 'unique','message' =>'mal'],
            [['name'], 'unique','message' =>'mal'],
            [['lastname'], 'unique','message' =>'mal'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusers' => 'Idusers',
            'name' => 'Name',
            'lastname' => 'Lastname',
            'username' => 'Username',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthassignments()
    {
        return $this->hasMany(Authassignment::className(), ['user_id' => 'idusers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(Authitems::className(), ['name' => 'item_name'])->viaTable('authassignment', ['user_id' => 'idusers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['id_user' => 'idusers']);
    }
}
