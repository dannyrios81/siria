<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Import_Fertilizantes".
 *
 * @property int $id
 * @property int $regi_venta
 * @property string $nomb_comer
 * @property string $Unid_Medid
 * @property string $tipo
 * @property string $clase
 * @property string $uso
 * @property string $paisorigen
 * @property int $iduser
 * @property int $idcompany
 * @property int $idestado
 * @property int $Column1
 *
 * @property Company $company
 * @property Estado $estado
 * @property Users $user
 */
class ImportFertilizantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Import_Fertilizantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['regi_venta', 'nomb_comer', 'Unid_Medid', 'tipo', 'clase', 'uso', 'paisorigen', 'iduser', 'idcompany', 'idestado', 'Column1'], 'required'],
            [['regi_venta', 'iduser', 'idcompany', 'idestado', 'Column1'], 'integer'],
            [['nomb_comer', 'Unid_Medid', 'tipo', 'clase', 'uso', 'paisorigen'], 'string', 'max' => 255],
            [['idcompany'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['idcompany' => 'idcompany']],
            [['idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['idestado' => 'idestado']],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['iduser' => 'idusers']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'regi_venta' => 'Regi Venta',
            'nomb_comer' => 'Nomb Comer',
            'Unid_Medid' => 'Unid Medid',
            'tipo' => 'Tipo',
            'clase' => 'Clase',
            'uso' => 'Uso',
            'paisorigen' => 'Paisorigen',
            'iduser' => 'Iduser',
            'idcompany' => 'Idcompany',
            'idestado' => 'Idestado',
            'Column1' => 'Column1',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['idestado' => 'idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'iduser']);
    }
}
