<?php

namespace app\models\entities\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\entities\Asignaciones as AsignacionesModel;

/**
 * Asignaciones represents the model behind the search form of `app\models\entities\Asignaciones`.
 */
class Asignaciones extends AsignacionesModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idcompany', 'nit', 'idusers', 'asignado'], 'integer'],
            [['name', 'categoria', 'archivo', 'fecha_cargue', 'estado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsignacionesModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idcompany' => $this->idcompany,
            'nit' => $this->nit,
            'idusers' => $this->idusers,
            'fecha_cargue' => $this->fecha_cargue,
            'asignado' => $this->asignado,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'archivo', $this->archivo])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
}
