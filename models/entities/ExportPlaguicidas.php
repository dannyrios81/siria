<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Export_Plaguicidas".
 *
 * @property int $id
 * @property int $regi_venta
 * @property string $nomb_comer
 * @property string $ingr_activ
 * @property string $concentra
 * @property string $clase
 * @property string $tipo
 * @property string $volumen
 * @property string $paisdestin
 * @property int $iduser
 * @property int $idcompany
 * @property int $idestado
 * @property int $nombre
 *
 * @property Company $company
 * @property Estado $estado
 * @property Users $user
 */
class ExportPlaguicidas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Export_Plaguicidas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['regi_venta', 'nomb_comer', 'ingr_activ', 'concentra', 'clase', 'tipo', 'volumen', 'paisdestin', 'iduser', 'idcompany', 'idestado', 'nombre'], 'required'],
            [['regi_venta', 'iduser', 'idcompany', 'idestado', 'nombre'], 'integer'],
            [['nomb_comer', 'ingr_activ', 'concentra', 'clase', 'tipo', 'volumen', 'paisdestin'], 'string', 'max' => 255],
            [['idcompany'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['idcompany' => 'idcompany']],
            [['idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['idestado' => 'idestado']],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['iduser' => 'idusers']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'regi_venta' => 'Regi Venta',
            'nomb_comer' => 'Nomb Comer',
            'ingr_activ' => 'Ingr Activ',
            'concentra' => 'Concentra',
            'clase' => 'Clase',
            'tipo' => 'Tipo',
            'volumen' => 'Volumen',
            'paisdestin' => 'Paisdestin',
            'iduser' => 'Iduser',
            'idcompany' => 'Idcompany',
            'idestado' => 'Idestado',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['idestado' => 'idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'iduser']);
    }
}
