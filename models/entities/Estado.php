<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Estado".
 *
 * @property int $idestado
 * @property string $nomestado
 *
 * @property BioinsumosExportacion[] $bioinsumosExportacions
 * @property FertilizantesProduccion[] $fertilizantesProduccions
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomestado'], 'required'],
            [['nomestado'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestado' => 'Idestado',
            'nomestado' => 'Nomestado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBioinsumosExportacions()
    {
        return $this->hasMany(BioinsumosExportacion::className(), ['idestado' => 'idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFertilizantesProduccions()
    {
        return $this->hasMany(FertilizantesProduccion::className(), ['idestado' => 'idestado']);
    }
}
