<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "Prod_Bioinsumos".
 *
 * @property int $id
 * @property int $regi_venta
 * @property string $nomb_comer
 * @property string $ingr_activ
 * @property string $clase
 * @property string $Unid_Medid
 * @property string $concentra
 * @property string $tipo
 * @property string $volu_prod
 * @property string $volu_vent
 * @property int $iduser
 * @property int $idcompany
 * @property int $idestado
 * @property int $nombre
 *
 * @property Company $company
 * @property Estado $estado
 * @property Users $user
 */
class ProdBioinsumos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Prod_Bioinsumos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['regi_venta', 'nomb_comer', 'ingr_activ', 'clase', 'Unid_Medid', 'concentra', 'tipo', 'volu_prod', 'volu_vent', 'iduser', 'idcompany', 'idestado', 'nombre'], 'required'],
            [['regi_venta', 'iduser', 'idcompany', 'idestado', 'nombre'], 'integer'],
            [['nomb_comer', 'ingr_activ', 'clase', 'Unid_Medid', 'concentra', 'tipo', 'volu_prod', 'volu_vent'], 'string', 'max' => 255],
            [['idcompany'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['idcompany' => 'idcompany']],
            [['idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['idestado' => 'idestado']],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['iduser' => 'idusers']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'regi_venta' => 'Regi Venta',
            'nomb_comer' => 'Nomb Comer',
            'ingr_activ' => 'Ingr Activ',
            'clase' => 'Clase',
            'Unid_Medid' => 'Unid Medid',
            'concentra' => 'Concentra',
            'tipo' => 'Tipo',
            'volu_prod' => 'Volu Prod',
            'volu_vent' => 'Volu Vent',
            'iduser' => 'Iduser',
            'idcompany' => 'Idcompany',
            'idestado' => 'Idestado',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['idcompany' => 'idcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['idestado' => 'idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['idusers' => 'iduser']);
    }
}
