<?php

namespace app\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\entities\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\entities\Users`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'lastname', 'username', 'password'], 'filter','filter' => 'strtolower'],
            [['idusers'], 'number'],
            [['name', 'lastname', 'username', 'password','authkey', 'token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$profile='')
    {

        if(!empty($profile))
            $query = Users::find()->joinWith('authassignments',true);
        else
            $query =Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idusers' => $this->idusers,
            'authassignment.item_name' => $profile,
        ]);

        $query->andFilterWhere(['like', 'LOWER(name)', $this->name])
            ->andFilterWhere(['like', 'LOWER(lastname)', $this->lastname])
            ->andFilterWhere(['like', 'LOWER(username)', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'authkey', $this->authkey])
            ->andFilterWhere(['like', 'token', $this->token]);

        return $dataProvider;
    }
}