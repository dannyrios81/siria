<?php
namespace app\models;

use yii\base\Model;

class carguearchivo extends Model
{
    public $file;
    public function rules()
    {
        return [
            [['file'], 'file',
                'skipOnEmpty' => false,
                'uploadRequired' => 'No has seleccionado ningún archivo',
                'maxSize' => 1024*1024*2,
                'tooBig' => 'El tamaño máximo permitido es 2 MB',
                'minSize' => 10,
                'tooSmall' => 'El tamaño mínimo permitido son {minSize} BYTES',
                'extensions' => 'xlsx, xls',
                'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}',
                'maxFiles' => 1,
                'tooMany' => 'El máximo de archivos permitidos son {limit}',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
          'file' => 'Archivos'
        ];
    }
}