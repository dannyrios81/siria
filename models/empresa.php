<?php
namespace app\models;

use app\models\entities\Authassignment;
use app\models\entities\Authitems;
use app\models\entities\Company;
use app\models\entities\UserCompany;
use app\models\entities\Users;
use yii\base\Model;
use \Exception;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class empresa extends Model
{
    public $nomemp;
    public $numide;
    public $matricula;
    public $telef;
    public $emai;
    public $dirrec;
    public $name;
    public $lastname;
    public $username;
    public $password;
    public $verif;
    private $session;

    public function __construct($config = [])
    {
        if(Yii::$app->session->has('infoRues'))
        {
            $this->session = Yii::$app->session->get('infoRues');
            $this->nomemp = $this->session['razon_social'];
            $this->numide = $this->session['numero_identificacion'];
            $this->dirrec = (!empty($this->session['direccion_fiscal']))?$this->session['direccion_fiscal']:'';
            $this->emai = (!empty($this->session['correo_electronico_fiscal']))?$this->session['correo_electronico_fiscal']:'';
            $this->telef = (!empty($this->session['telefono_fiscal_1']))?$this->session['telefono_fiscal_1']:'';
        }
        else
            throw new Exception('variable de sesion no encontrada',001);

        parent::__construct($config);
    }

    public function rules()
    {
        return[
        [['nomemp','numide','matricula','telef','dirrec','emai','password','verif','name','lastname','username','password','verif'],'required', 'message' => 'El {attribute} es requerido'],
        [['name','lastname'],'match','pattern'=>'/^[a-zA-Z\s]+$/','message'=>'El campo {attribute} solo ingrese letras'],
        ['numide', 'match','pattern'=>'/^.{3,14}$/','message'=>'El numero de registro debe tener minimo 3 caracteres y maximo 14'],
        [['numide','matricula','telef'], 'integer', 'message' => 'El campo {attribute} solo numeros enteros'],
        [['emai','username'], 'email','message'=>'ejemplo@dominio.com'],
        ['verif','compare','compareAttribute'=>'password','message'=>'La contraseña y la verificacion de contraseña no coinciden'],
        ['password','match','pattern'=>'/^(?=.*\d)(?=.*[@#\-_*$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_*$%^&+=§!\?]{8,20}$/','message'=>'El campo {attribute} requiere minimo una letra mayuscula, una letra minuscula, un digito, un caracter especial y una longitud minima de 8 caracteres y maxima de 20.'],
        [['username'],'unique','targetClass'=>Users::className(),'targetAttribute'=>'username'],
        [['emai'],'unique','targetClass'=>Company::className(),'targetAttribute'=>'email'],
        [['numide'],'unique','targetClass'=>Company::className(),'targetAttribute'=>'nit'],
        ['matricula', 'validateMatricula']
        ];
    }

    public function attributeLabels()
    {
        return[
            'nomemp' => 'Razón social',
            'tipide' => 'Tipo de identificación',
            'numide' => 'Número tributario',
            'matricula' => 'Matricula mercantil',
            'telef' => 'Telefóno contacto',
            'emai' => 'Email contacto empresa',
            'dirrec' => 'Direccion empresa',
            'vincu' => 'Nombre completo representante',
            'username' => 'Email representante',
            'verif' => 'Verificacion de contraseña',
            'name' => 'Nombres representate',
            'lastname' => 'Apellidos representante',
            'password' => 'Digite contraseña',
            'Verif' => 'Verificacion contraseña'
        ];
    }

    public function save()
    {
        $ruta = Yii::getAlias('@xlsupload'.'/');
        $retorno = false;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $user = new Users();
            $user ->name = $this->name;
            $user ->lastname = $this->lastname;
            $user ->username = $this->username;
            $user ->password = $this->password;
            $user->save();

            $company = new Company();
            $company->name = $this->nomemp;
            $company->nit = $this->numide;
            $company->phone = $this->telef;
            $company->address = $this->dirrec;
            $company->email = $this->emai;
            $company->save();

            $user_company = new UserCompany();
            $user_company->id_user = $user->idusers;
            $user_company->id_company = $company->idcompany;
            $user_company->Estado_usuario = 1;
            $user_company->save();



            // forma aceptable de asignar permisos(Perfil) a un usuario
            /*$temp = new Authassignment();
            $temp->item_name = "Administrador";
            $temp->user_id = $user->idusers;
            $temp->save();*/

            // forma correcta de asignar permisos(Perfil) a un usuario
            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('Representante Legal');
            $auth->assign($authorRole, $user->idusers);

            $transaction->commit();
            $retorno= true;
        }
        catch (Exception $e)
        {
            $transaction->rollBack();
        }
        $dir = $ruta . $company->idcompany;
        mkdir($dir,'0755');
        return $retorno;
    }

    public function validateMatricula($attribute, $params)
    {
        $MatriculaEncontrada =false;
        foreach ($this->session['matricula'] as $matricula) {
            if($this->$attribute == $matricula)
                return;
        }
        $this->addError($attribute,'La '.$this->attributeLabels()[$attribute].' No Es Correcta');
    }
}