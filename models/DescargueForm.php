<?php
namespace app\models;

use Yii;
use yii\base\Model;

class DescargueForm extends Model
{

    public function Descargar()
    {
        $path = Yii::getAlias('@xlsdownload');
        if(1 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ImportacionFertilizantes.xlsx');
        }
        elseif (2 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ProduccionFertilizantes.xlsx');
        }
        elseif (3 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ExportacionFertilizantes.xlsx');
        }
        elseif (4 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ImportacionBioinsumos.xlsx');
        }
        elseif (5 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ProduccionBioinsumos.xlsx');
        }
        elseif (6 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ExportacionBioinsumos.xlsx');
        }
        elseif (7 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ImportacionPlaguicidas.xlsx');
        }
        elseif (8 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ProduccionPlaguicidas.xlsx');
        }
        elseif (9 === Yii::$app->session->get('tipo'))
        {
            return Yii::$app->response->sendFile($path.'/'.'ExportacionPlaguicidas.xlsx');
        }else{
            Yii::$app->session->setFlash('error','no se puede descargar el excel.');
            return false;
        }

    }
}