<?php
namespace app\models;

use yii\base\BaseObject;
use app\models\entities\Post;

class modelqueue extends BaseObject implements \yii\queue\JobInterface
{
    public $title;
    public $Body;

    public function execute($queue)
    {
        $faker = \Faker\Factory::create();

        $post = new Post();
        for($i = 1; $i <=50; $i++){
            $post->setIsNewRecord(true);
            $post->id = null;
            $post->title = $faker->words(1,true);
            $post->Body = $faker->paragraph(1);
            $post->save();
        }
    }
}