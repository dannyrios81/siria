<?php
namespace app\models;

use app\models\entities\Company;
use yii\base\Model;

class rues extends Model
{
    public $campo;
    public $verif;

    public function rules()
    {
        return[
            [['campo','verif'],'required','message'=>'El {attribute} es requerido'],
            ['campo', 'integer', 'message' => 'Sólo números enteros'],
            [['campo'],'match','pattern'=>'/^.{3,14}$/','message'=>'El numero de registro debe tener minimo 3 caracteres y maximo 14'],
            ['verif', 'integer', 'message' => 'Sólo números enteros'],
            [['verif'],'match','pattern'=>'/^.{1,1}$/','message'=>'El codigo de verificacion solo es de un digito'],
            [['verif','campo'],'safe'],
            [['campo'],'unique','targetClass'=>Company::className(),'targetAttribute'=>'nit']
        ];
    }

    public function attributeLabels()
    {
        return [
            'verif' => 'Digito de verificación',
            'campo' => 'Número único tributario'
        ];
    }
}