<?php
namespace app\models\forms;

use app\models\entities\UserCompany;
use app\models\entities\Users;
use Yii;
use yii\base\model;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

class FormRecoverPass extends model
{

    public $username;
    public $respuesta;





    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Campo requerido'],
            ['username', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['username', 'email', 'message' => 'Formato no válido'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Correo usuario'
        ];
    }

    public  function recuperar()
    {
        $rnd = Yii::$app->getSecurity()->generateRandomString(11);
        $usu = Users::find()->where('username = :usnam')->params([':usnam' => $this->username])->all();
        foreach ($usu as $cambio)
        {
            $respuesta['idusers'] = $cambio['idusers'];
        }
        if(isset($respuesta['idusers']))
        {
        if(isset($usu))
        {
            $otrorandom = UserCompany::find()->where('id_user = :asd')->params([':asd' => $respuesta['idusers']])->one();
            $otrorandom->Random = $rnd;
            $otrorandom->save();
                Yii::$app->mailer->compose()
                    ->setTo($this->username)
                    ->setFrom('Notificaciones@ica.gov.co')
                    ->setSubject('Codigo de Verificacion')
                    ->setHtmlBody('<html><head><h1>Reinicio de contraseña.</h1></head><body>Buen día.<br /><br />Apreciado Usuario.<br /><br />El numero de verificacion para el cambio de contraseña es: '.$rnd.' <br /><br />Muchas gracias por su colaboración</body></html>')
                    ->send();
                Yii::$app->session->setFlash('success','Correo electrónico enviado con el número de verificación.');
                return true;
        }
        else
        {
            Yii::$app->session->setFlash('error','Correo electrónico no registrado con anterioridad');
        }
        }
        else
        {
            Yii::$app->session->setFlash('error','Correo electrónico no registrado en la base de datos');
        }
    }
}