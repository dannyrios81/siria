<?php

namespace app\models\forms;

use yii\base\Model;

class consultasForm extends Model{
    public $company;
    public $nit;

    public function rules(){
        return[
          ['company', 'match', 'pattern' =>'/^[a-zA-Z\s]+$/','message'=>'Solo letras por favor'],
          ['nit', 'integer','message'=>'Solo numero por favor'],
        ];
    }

    public function attributeLabels(){
        return[
           'company'=>'Nombre de la empresa',
           'nit'=>'Nit'
        ];
    }
}
