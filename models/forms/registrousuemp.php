<?php
namespace app\models\forms;
use app\models\entities\UserCompany;
use app\models\entities\Users;
use Yii;
use yii\base\Model;

class registrousuemp extends Model
{
    //put your code here

    public $name;
    public $username;
    public $password;
    public $lastname;
    public $confirmar;
    public $random;

    public function rules()
    {
        return [
            [['name', 'username', 'confirmar','lastname'], 'required', 'message' => 'Ingrese el {attribute} por favor'],
            [['password'], 'required', 'message' => 'Ingrese la {attribute} por favor'],
            ['confirmar', 'compare', 'compareAttribute' => 'password', 'message' => 'No coincide la contraseña'],
            ['username', 'email', 'message' => 'ejemplo@dominio.com'],
            ['name', 'match', 'pattern' => '/^[a-zA-Z;ñ\s]+$/', 'message' => 'Solo letras por favor'],
//                  ['password', 'validatePassword'],
            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[@#\-_*$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_*$%^&+=§!\?]{6,15}$/', 'message' => 'La Contraseña debe tener minimo 6 caracteres, por lo menos una mayuscula, un numero o un caracter especial (!@#$&*)'],
            ['random', 'required', 'message' => 'Digite el código de verificación enviado mediante el correo.'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'random' =>'Código de verificación',
            'name' => 'Nombre del usuario',
            'username' => 'Correo electrónico',
            'lastname' =>'Apellido del usuario',
            'password' => 'Contraseña',
            'confirmar' => 'Confirmar contraseña',
        ];
    }

    public function save()
    {
        if($iduse=Users::find()->select('idusers')->where('username = :usnam')->params([':usnam' => $this->username])->one()){
            if(($random=UserCompany::find()->select('random')->where('idusers' == $iduse)->one()))
            {
                $Users = Users::find()->select('idusers')->where('username = :usnam')->params([':usnam' => $this->username])->one();
                $Users->name = $this->name;
                $Users->lastname = $this->lastname;
                $Users->password = $this->password;
                return $Users->save();
            }
            else
            {
                Yii::$app->session->setFlash('error','Digito Incorrecto.');
            }
        }
        else
        {
            Yii::$app->session->setFlash('error', 'El correo no se encuentra registrado con anterioridad.');
            return false;
        }
    }
    /*
        public function validatePassword($attribute, $params)
        {
            $authLdap = false;
            $disableLdap = \Yii::$app->params['disableLdap'];
            $user = $this->getUser();
            if ($disableLdap) {
                if (empty($user))
                    $this->addError('username', 'Usuario no encontrado');
            } else {
                if (!$this->hasErrors()) {
                    if (!$user) {
                        $this->addError('username', 'El Usuario Ingresado No Se Encuentra Autorizado Para Usar Esta Aplicación');
                        return false;
                    }
                    if (!empty($user) && Ldap::AuthUsuarioDirect($this->username, $this->password))
                        $authLdap = true;
                    else
                        $this->addError('username', 'El Usuario y/o Password Son Incorrectos.');
                }
            }
        }

        public function login()
        {
            if ($this->validate()) {
                return Yii::$app->user->login($this->getUser(), 0);
            }
            return false;
        }

        public function getUser()
        {
            if ($this->_user === false) {
                $this->_user = Users::findByUsername($this->username);
            }
            return $this->_user;
        }
    */
}