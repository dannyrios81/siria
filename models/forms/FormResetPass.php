<?php

namespace app\models\forms;

use app\models\entities\UserCompany;
use app\models\entities\Users;
use Yii;
use yii\base\model;
class FormResetPass extends model{

    public $email;
    public $password;
    public $password_repeat;
    public $verification_code;

    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'verification_code'], 'required', 'message' => 'Ingrese el {attribute} por favor'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[@#\-_*$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_*$%^&+=§!\?]{6,15}$/', 'message' => 'La Contraseña debe tener minimo 6 caracteres, por lo menos una mayuscula, un numero o un caracter especial (!@#$&*)'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
        ];


    }

    public function attributeLabels()
    {
        return [
            'verification_code' =>'Código de verificación',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'password_repeat' => 'Confirmar contraseña',
        ];
    }

    public function cambiar()
    {
        $usu = Users::find()->where('username = :usnam')->params([':usnam' => $this->email])->all();
        foreach ($usu as $cambio)
        {
            $respuesta['idusers'] = $cambio['idusers'];
        }
        if(isset($usu))
        {
            $random = UserCompany::find()->select('Random')->where('id_user = :asd')->params([':asd' => $respuesta['idusers']])->one();
            if(!$this->verification_code === $random)
            {
                Yii::$app->session->setFlash('error','Número de verificación erroneo.');
                return false;
            }
            else
            {
                $hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                $guardar = Users::find()->where('idusers = :asd')->params([':asd' => $respuesta['idusers']])->one();
                $guardar->password = $hash;
                $guardar->save();
                Yii::$app->mailer->compose()
                    ->setTo($this->email)
                    ->setFrom('Notificaciones@ica.gov.co')
                    ->setSubject('Cambio de contraseña')
                    ->setHtmlBody('<html><head><h1>Cambio de contraseña.</h1></head><body>Buen día.<br /><br />Apreciado Usuario.<br /><br />Su contraseña fue cambia correctamente<br /><br /></body></html>')
                    ->send();
                Yii::$app->session->setFlash('success','Contraseña cambiada correctamente.');
                return true;
            }
        }
       else
        {
            Yii::$app->session->setFlash('error','Correo electrónico no registrado con anterioridad');
        }
        return true;
    }
}
