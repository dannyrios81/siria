<?php
namespace app\models\forms;

use app\components\Ldap;
use app\models\entities\UserCompany;
use app\models\entities\Users;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;
    public $verification_code;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username','filter','filter' => 'strtolower'],
            // username and password are both required
            [[ 'password','username'], 'required','message' => 'Por favor digitar el {attribute} '],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Contraseña',
            'username' => 'Usuario',


        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        #VarDumper::dump(preg_match('/[@]/',$this->username),10,1);
        #exit;
        if(preg_match('/[@]/',$this->username)){
            //usuario externo
            $disableLdap = \Yii::$app->params['disableLdap'];
            $user = $this->getUser();
            if($disableLdap)
            {
                if(empty($user))
                    $this->addError('username','Usuario no existe');
            }
            else
            {
                if (!$this->hasErrors()) {
                    if(!$user)
                    {
                        $this->addError('username','El Usuario Ingresado No Se Encuentra Autorizado Para Usar Esta Aplicación');
                        return false;
                    }
                    elseif(!empty($user))
                    {
                        $habilitado = UserCompany::find()->select('Estado_usuario')->where(['id_user'=>$user->idusers])->one();
                        if($habilitado['Estado_usuario'] === 1)
                        {
                            $hash = $user->password;
                            if(password_verify($this->password,$hash)){
                                Yii::$app->session->setFlash("success", "Ingreso correctamente");
                            }
                            else
                            {
                                $this->addError( 'username', 'La  Contraseña es incorrecta.');
                            }
                        }
                        else
                        {
                            $this->addError( 'username', 'Usuario inhabilitado.');
                        }
                    }
                    else
                    {
                        $this->addError( 'username', 'Contraseña Incorrecta.');
                    }
                }
            }
        }
        else
        {
        //usuario ICA
        $disableLdap = \Yii::$app->params['disableLdap'];
        $user = $this->getUser();
        if($disableLdap)
        {
            if(empty($user))
                $this->addError('username','Usuario no existe');
        }
        else
        {
            if (!$this->hasErrors()) {

                if(!$user)
                {
                    $this->addError('username','El usuario ingresado no se encuentra autorizado para usar esta aplicación');
                    return false;
                }

                if(!empty($user) && Ldap::AuthUsuarioDirect($this->username,$this->password))
                    $authLdap = true;
                else
                    $this->addError('username','El usuario y/o password son incorrectos.');

            }
        }

        }

    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully  
     */
    public function login()
    {
        if ($this->validate())
        {
            return Yii::$app->user->login($this->getUser(), 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Users|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->username);
        }
        return $this->_user;
    }
}

