<?php
namespace app\models\forms;

use app\models\entities\UserCompany;
use app\models\entities\Customer;
use Yii;
use yii\base\Model;
use \Exception;
use yii\db\ActiveRecord;
use yii\bootstrap\Alert;
use yii\bootstrap\Widget;
use yii\helpers\VarDumper;
use app\models\entities\Users;


class crearusuempForm extends Model {

    public $name;
    public $lastname;
    public $username;

    public function rules()
    {
        return [
            
            [['name','lastname','username'], 'required','message' => 'Por favor digitar el {attribute} '],
            ['username','email','message'=>'ejemplo@dominio.com'],
            ['name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/','message' => 'Solo letras por favor'],
            ['lastname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/','message' => 'Solo letras por favor'],
            [['username'],'unique','targetClass'=>Users::className(),'targetAttribute'=>'username'],
        ];
    }
    
     public function attributeLabels()
    {
        return [
            'username' => 'Correo electrónico',
            'name' => 'Nombre',
            'lastname' => 'Apellido',
        ];
    }
    
 
     public function save()
    {
        $rando = Yii::$app->getSecurity()->generateRandomString(11);
        $retorno = false;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $customer = new Users();
            $customer->lastname = $this->lastname;
            $customer->username= $this->username;
            $customer->name = $this->name;
            $customer->save(false);

            $usercompany = new UserCompany();
            $usercompany->id_user= $customer->idusers;
            $usercompany->id_company=Yii::$app->session->get("idcompania");
            $usercompany->Estado_usuario = 1;
            $usercompany->Random = $rando;
            $usercompany->save(false);

             $auth = Yii::$app->authManager;
             $authorRole = $auth->getRole('Delegado Responsable');
             $auth->assign($authorRole, $customer->idusers);

            $transaction->commit();
            $retorno= true;
        }
        catch (Exception $e)
        {
            $transaction->rollBack();
        }
        Yii::$app->mailer->compose()
            ->setTo($this->username)
            ->setFrom('Notificaciones@ica.gov.co')
            ->setSubject('Seleccion Delegado')
            ->setHtmlBody('<html><head><h1>Bienvenido Apreciado Usuario.</h1></head><body>Buen día.<br /><br />Ha sido seleccionado como el delegado responsable de colaborar en el cargue de la informacion para ser enviada al registro del ICA.<br /> Para registrarse correctamente use el siguiente codigo de verificacion: '.$rando.' en http://192.168.1.24/siria/web/prueba/registrousuemp <br /><br /></body></html>')
            ->send();
        return $retorno;
    }

    public function cantidadmaximausuarios()
    {
        //el id de la compañia tiene que ir por sesion
        $idcompany = Yii::$app->session->get("idcompania");
        //VarDumper::dump(UserCompany::find()->where('id_company = :idcom')->params([':idcom'=>$idcompany]),10,true);
        $cuentausu =  UserCompany::find()->where('id_company = :idcom')->params([':idcom'=>$idcompany])->count();
//        VarDumper::dump($cuentausu,10,true);exit;
        return $cuentausu;
    }
}