<?php


namespace app\models\forms;


use yii\base\Model;

class asignacionForm extends Model {

    public $company;
    public $nit;
    public $asignados;

    public function rules()
    {
        return [


            ['company', 'match', 'pattern' => '/^[a-zA-Z\s]+$/','message' => 'Solo letras por favor'],
            ['nit', 'integer','message' => 'Solo numeros por favor'],

        ];
    }


    public function attributeLabels()
    {
        return [
            'company'=>'Nombre empresa',
            'nit'=>'Nit',
            'asignados'=>'Asignados'

        ];
    }

}