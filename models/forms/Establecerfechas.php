<?php
namespace app\models\forms;

use app\models\entities\Company;
use app\models\entities\Fechas;
use app\models\entities\UserCompany;
use yii\base\model;
use Yii;
use yii\helpers\VarDumper;

class Establecerfechas extends model
{

    public $fechainicio;
    public $fechafin;
    public $nit;
    public $nombre;


    public function save(){
        $xyz = Fechas::find()->where('id = 1')->one();
        $xyz->fecha_inicio = $this->fechainicio;
        $xyz->fecha_fin = $this->fechafin;
        return $xyz->save();
    }
    
    public function rules()
    {
        return [
            [['fechainicio', 'fechafin'], 'required', 'message' => 'Campo requerido']
        ];
    }

    public function attributeLabels()
    {
        return [
            'fechainicio' => 'Fecha inicio del cargue',
            'fechafin' => 'Fecha fin del cargue',
            'nombre'=> 'Nombre Empresa',
            'Nit'=> 'Nit'
        ];
    }

    public function addTime(){
        $customer = new Fechas();
        $customer->fecha_inicio = $this->fechainicio;
        $customer->fecha_fin = $this->fechafin;
        $customer->nit = $this->nit;
        $customer->razon = $this->razon;
        return $customer->save();

    }
}