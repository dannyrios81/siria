<?php
namespace app\models\forms;

use app\models\entities\Users;
use yii\base\Model;
use yii\db\Exception;
use Yii;

class Formdelegadoica extends Model
{
    public $dominio;
    public $correo;
    public $name;
    public $lastname;

    public function __construct($config = [])
    {
        $this->correo = '@ICA.GOV.CO';
    }

    public function rules()
    {
        return[
            [['dominio','name','lastname'],'required', 'message'=>'El campo {attribute} es requerido.'],
            [['dominio'],'unique','targetClass'=>Users::className(),'targetAttribute'=>'username'],
            ['dominio','match','pattern'=>'/^[a-z]{1,}.[a-z]{1,}$/', 'message'=>'Se requiere un {attribute} valido.'],
            [['name','lastname'],'match','pattern'=>'/^[a-z;A-Z;ñ; ]{3,}$/', 'message'=>'Se requiere un {attribute} valido.']
        ];
    }

    public function attributeLabels()
    {
        return[
            'dominio'=>'Usuario Dominio ICA',
            'correo'=>'Dominio ICA',
            'name'=>'Nombre',
            'lastname'=>'Apellidos'
        ];
    }

    public function save()
    {
        $retorno = false;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $usuario = new Users();
            $usuario->name = $this->name;
            $usuario->lastname = $this->lastname;
            $usuario->username = $this->dominio;
            $usuario->save();

            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('Apoyo');
            $auth->assign($authorRole, $usuario->idusers);

            $transaction->commit();
            $retorno= true;
        }catch(Exception $e)
        {
            Yii::$app->session->setFlash("error",$e);
            $transaction->rollBack();
        }
        return $retorno;
    }
}