<?php
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($model, 'file')->fileInput() ?>
    <button class="btn-success btn btn-primary">Subir archivos</button>

<?php ActiveForm::end() ?>