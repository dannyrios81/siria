<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin(['method'=>'POST']);?>
<div class="input-group mb-3" >
<table class="table ">
<tr><td>
    <?= $form->field($model, 'company')->textInput(['maxlength'=>60,'placeholder'=>'Empresa','class'=>'form-control']) ?>
</td></tr>
<tr><td>
    <?= $form->field($model, 'nit')->textInput(['maxlength'=>10,'placeholder'=>'Nit','class'=>'form-control']) ?>
</td></tr>

<tr><td>

        <label class="control-label">Asignaciones</label>
        <br />
            <select class="table table-striped form-control">
               <option value = "0"></option>
                <option value = "1">Asignados</option>
                <option value = "2">Sin Asignar</option>
            </select>


</td></tr>

</table>
</div>
<br>
    <?= Html::submitButton("Buscar solicitud", ["class" => "btn btn-success"]) ?>
    <?= Html::submitButton("Limpiar", ["class" => "btn btn-success"]) ?>

<?php ActiveForm::end();?>