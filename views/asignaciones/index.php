<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;


/* @var $model app\models\forms\asignacionForm */
/* @var $this yii\web\View */
/* @var $searchModel app\models\entities\search\Asignaciones */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asignaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Asignaciones', ['create'], ['class' => 'btn btn-success']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'nit',
                'header' => 'N° Identificacion'
            ],
            [
                'attribute'=>'name',
                'header' => 'Nombre Empresa'
            ],
            [
                'attribute'=>'idusers',
                'header' => 'Codigo Usuario'
            ],
            [
                'attribute'=>'categoria',
                'header' => 'Categoria'
            ],
            [
                'attribute'=>'archivo',
                'header' => 'Nombre archivo'
            ],
            [
                'attribute'=>'fecha_cargue',
                'header' => 'Fecha Cargue'
            ],
            [
                'attribute'=>'estado',
                'header' => 'Estado'
            ],
            [
                'attribute'=>'asignar',
                'header' => 'Asignar',

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],


        ],
    ]); ?>


</div>

<?php //$form = ActiveForm::begin(['method'=>'POST']);?>
<!---->
<?//= $form->field($model, 'company')->textInput(['maxlength'=>60,'placeholder'=>'company']) ?>
<?//= $form->field($model, 'nit')->textInput(['maxlength'=>60,'placeholder'=>'nit']) ?>
<!--<input type="checkbox" id="">-->
<!--    --><?//= Html::submitButton("Buscar", ["class" => "btn btn-success"]) ?>
<!--    --><?//= Html::submitButton("Limpiar", ["class" => "btn btn-success"]) ?>
<!---->
<!--</div>-->
<?php //ActiveForm::end();?>
