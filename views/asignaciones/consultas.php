<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin(['method' => 'POST']); ?>
    <div class="input-group mb-3">
        <table class="table table-striped">
            <tr><td>
                    <?= $form->field($model, 'company')->textInput(['maxlength'=>40,'placeholder'=>'Empresa','class'=>'form-control'])?>
                </td><td>
            <tr><td>
                    <?=  $form->field($model, 'nit')->textInput(['maxlength'=>15,'placeholder'=>'Nit','class'=>'form-control'])?>
                </td></tr>
        </table>
    </div>
<?=Html::submitButton('Buscar', ['class'=>'btn btn-success'])?>

<?=Html::submitButton('Limpiar', ['class'=>'btn btn-success'])?>

<?php ActiveForm::end();?>


