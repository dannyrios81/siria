<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\entities\search\Asignaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idcompany') ?>

    <?= $form->field($model, 'nit') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'idusers') ?>

    <?php // echo $form->field($model, 'categoria') ?>

    <?php // echo $form->field($model, 'archivo') ?>

    <?php // echo $form->field($model, 'fecha_cargue') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'asignado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
