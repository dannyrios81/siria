<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\entities\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empresas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                    'attribute'=>'name',
                    'header' => 'Nombre'
            ],
            [
                'attribute'=>'nit',
                'header' => 'N° Identificación'
            ],
            [
                'attribute'=>'phone',
                'header' => 'Telefono'
            ],
            [
                'attribute'=>'address',
                'header' => 'Dirección'
            ],
            [
                'attribute'=>'email',
                'header' => 'E-mail',
                'format' => 'email'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>


</div>
