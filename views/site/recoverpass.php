<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Recuperar Contraseña</h1>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>

<div class="form-group">
    <?= $form->field($model, "username")->input("email") ?>
</div>

<?= Html::submitButton("Recuperar.", ["class" => "btn btn-success"]) ?>

<?php $form->end() ?>
