<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<h1>Recuperar Contraseña</h1>
<?php $form = ActiveForm::begin(['method' => 'post']);?>
<div class="form-group"><?= $form->field($model, "email")->input("email") ?></div>

<div class="form-group"><?= $form->field($model, "password")->input("password") ?></div>

<div class="form-group"><?= $form->field($model, "password_repeat")->input("password") ?></div>

<div class="form-group"><?= $form->field($model, "verification_code")->input("text") ?></div>

<?= Html::submitButton("Recuperar Contraseña", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>
