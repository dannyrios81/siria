<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="container-fluid main-bg-landing">
    <br /><h1 class="text-center">Bienvenidos a S.I.R.I.A</h1><h4 class="text-center">Sistema de Información para el Reporte de Insumos Agrícolas.</h4><br /><br />
    <div class="row">
        <div class="col-lg-4">
            <h2 class="text-center">Plaguicida</h2>
            <p class="text-center"><a class="btn btn-default btn-success" href="https://www.ica.gov.co/areas/agricola/servicios/regulacion-y-control-de-plaguicidas-quimicos.aspx" target="_blank">Descripción &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2 class="text-center">Bioinsumos</h2>
            <p class="text-center"><a class="btn btn-default btn-success" href="https://www.ica.gov.co/preguntas-frecuentes/agricola/bioinsumos"  target="_blank">Descripción &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2 class="text-center">Fertilizantes</h2>
            <p class="text-center"><a class="btn btn-default btn-success" href="https://www.ica.gov.co/getdoc/a5c149c5-8ec8-4fed-9c22-62f31a68ae49/fertilizantes-y-bio-insumos-agricolas.aspx"  target="_blank">Descripción  &raquo;</a></p>
        </div>
    </div>
</div>