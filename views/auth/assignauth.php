<?php

use app\components\Utilities;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \app\models\entities\Users */

$this->title = 'Asignación De Perfiles';
$this->params['breadcrumbs'][] = ['label' => 'Seguridad Y Perfiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title.' : '.$model->name . ' ' . $model->lastname;
$counter = 0;
?>
<div class="users-authassigment">
    <div class="panel panel-default">
        <div class="panel-heading"><h2 style="text-align: center">Usuario</h2></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Nombre Usuario : </strong><?php echo $model->name  ?></p>
                </div>
                <div class="col-md-6">
                    <p><strong>Apellido Usuario : </strong><?php echo $model->lastname; ?></p>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <h2 style="text-align: center">Roles</h2>
            <div class="row">
                <?php foreach ( Utilities::arrayRoles() as $item):?>
                <?php //\yii\helpers\VarDumper::dump(Utilities::arrayRoles(),10,true);exit; ?>
                <?php 
                $disabled = in_array($item, Yii::$app->params['profilesDisabled']);
                $enable = Yii::$app->authManager->checkAccess($model->idusers,$item) ?>
                <?php if(!$disabled): ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <?= Html::a(
                            ucwords($item),
                        Url::to(['auth/assignrevoke','id' => $model->idusers, "auth" => $item]),
                        ['class'=>'btn btn-' . ($enable ? 'success' : 'default').' btn-block'])?>
                </div>
                <?php $counter = $counter+1?>
                <?php endif; ?>
                <?php if($counter%3==0): ?>

            </div>
            <div class="row">
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
