<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchModels\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJs(<<< EOT_JS
$(document).on("click",".login-option", function(){
    $.ajax({
            url:'save-login',
            dataType:'text',
            type:'post',
            data:{id:this.id},
        }).done(function(response){
            $.pjax.reload({container:"#grid-users"});
        });
});
EOT_JS
,View::POS_END);
$this->title = 'Seguridad Y Perfiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<?php Pjax::begin(["id"=>"grid-users"]);?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'layout' => '{items}',
        'responsiveWrap' => false,
        'containerOptions'=>['style'=>'min-height:700px'],
        'columns' => [

                [
                    'attribute'=>'name',
                    'headerOptions'=>['style'=>'widht:12%'],
                ],
                [
                    'attribute'=>'lastname',
                    'headerOptions'=>['style'=>'widht:12%'],
                ],
                [
                    'attribute'=>'username',
                    'format'=>'email',
                ],
            [
                'class' => 'kartik\grid\ActionColumn',
                //                'template'=>'{assignrevokeauth}{assignrevokevalidates}{assignrevokesectionals}{assignrevokedependency}{update}',
                'template'=>'{assignrevokeauth}',
                'dropdown' => true,
                'dropdownOptions' => ['class' => 'pull-left', 'style'=>'position: initial;'],
                'dropdownMenu'=>['style'=>'position: initial;'],
                'buttons' => [
                    'assignrevokeauth'=>function ($url,$data,$key)
                    {
                        return Html::tag('li',
                            Html::a('<span class="glyphicon glyphicon-user"></span> Asignar o Revocar Permisos',
                                Url::to(['/auth/assignauth',"id"=>$data->idusers])));
                    },
                ]
            ],
        ],
    ]); 
    Pjax::end()
    ?>
</div>
