<?php
/* @var $model app\models\editarinfoForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div>
    <h1>Actualizar datos.</h1>
    <?php $form = ActiveForm::begin()?>
    <?= Html::errorSummary($model,['class'=>'alert alert-block alert-danger']) ?>

        <div class="form-inline align-items-center">
            <div class="col-auto">
                <select name="dato">
                    <option value="">Metodo de Busqueda</option>
                    <option value="A">Nit.</option>
                    <option value="B">Nombre empresa.</option>
                </select>

            </div>
            <div class="col-auto">
                <?= $form->field($model, 'buscar')->textInput(['placeholder' => $model->getAttributeLabel('buscar'),'class'=>'form-control mb-2'])?>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-2">Buscar.</button>
            </div>
        </div>
    <div class="form-group">
        <br>
        <br>
        <?= $form->field($model, 'nomemp')->textInput(['placeholder' => $model->getAttributeLabel('nomemp'),'disabled'=>'disabled'])?>
        <br>
        <?= $form->field($model, 'numide')->textInput(['placeholder' => $model->getAttributeLabel('numide'),'disabled'=>'disabled'])?>
        <br>
        <?= $form->field($model, 'telef')->textInput(['placeholder' => $model->getAttributeLabel('telef')])?>
        <br>
        <?= $form->field($model, 'dirrec')->textInput(['placeholder' => $model->getAttributeLabel('dirrec')])?>
        <br>
        <?= $form->field($model, 'emai')->textInput(['placeholder' => $model->getAttributeLabel('emai')])?>
    </div>
    <button type="submit" class="btn btn-primary">Guardar.</button>
    <?php ActiveForm::end();?>
</div>


