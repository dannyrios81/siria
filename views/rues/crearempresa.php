<?php
    /* @var $model app\models\rues  */
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Modal;
    use yii\web\View;

    if($supuesto){
        $script = <<< JS
        $("#Correcto").modal("show");
JS;
        $this->registerJs($script, View::POS_END);
    }
?>

<h1>Verificar NIT.</h1>
<div id='div-alfa' class='preloader-alfa'></div>
<?php $form = ActiveForm::begin(['method'=>'POST']);?>
<?= Html::errorSummary($model,['class'=>'alert alert-block alert-danger']) ?>
<div><br />
    <table class="table table-borderless table-responsive-lg">
        <tr>
            <th><?= $form->field($model, 'campo')->textInput(['placeholder' => $model->getAttributeLabel('campo'),'maxlength'=>14,'class'=>'form-control'])?></th>
            <th><?= $form->field($model, 'verif')->textInput(['placeholder' => $model->getAttributeLabel('verif'),'maxlength'=>1])?></th>
        </tr>
    </table>
    <?= Html::submitButton("Verificar", ['onclick' => '$("#div-alfa").slideDown("fast")',"class" => "btn btn-success"]) ?>
</div>
<?php ActiveForm::end();?>

<?php
Modal::begin([
    'id'=>'Correcto',
    'header' => 'Datos empresa'
]);
if($data != array()):
?>
    <div><div>
        <label>El <?= $data['tipo_identificacion']." ".$data['numero_identificacion'] ?> corresponde a <?=$data['razon_social']?>.</label><br /><br />
        <label>¿Estos datos Son Correctos?</label>
    </div><br /><br /><br />
    <div align='right'>
        <?= Html::a('Si.',['/rues/empresa'],['class'=>'btn btn-primary btn-success'])?>
        <!--            La Url aparecia en get por la variable que estaba enviando desde la linea de encima-->
        <button class='btn btn-primary btn-info' data-dismiss="modal">No.</button>
    </div></div>
<?php
endif;
Modal::end();
?>