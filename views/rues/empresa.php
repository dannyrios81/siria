<?php
/* @var $model app\models\empresa */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
?>

<div>
    <h1>Datos Empresa.</h1>
    <?php $form = ActiveForm::begin(['method'=>'POST']);?>
    <?= Html::errorSummary($model,['class'=>'alert alert-block alert-danger']) ?>
    <div class="form-group">
        <?= $form->field($model, 'nomemp')->textInput(['placeholder' => $model->getAttributeLabel('nomemp'),'disabled'=>'disabled'])?>
        <br>
        <?= $form->field($model, 'numide')->textInput(['placeholder' => $model->getAttributeLabel('numide'),'disabled'=>'disabled'])?>
        <br>
        <?= $form->field($model, 'matricula')->textInput(['placeholder' => $model->getAttributeLabel('matricula')])?>
        <br>
        <?= $form->field($model, 'telef')->textInput(['placeholder' => $model->getAttributeLabel('telef')])?>
        <br>
        <?= $form->field($model, 'dirrec')->textInput(['placeholder' => $model->getAttributeLabel('dirrec')])?>
        <br>
        <?= $form->field($model, 'emai')->textInput(['placeholder' => $model->getAttributeLabel('emai')])?>
        <br><br>
        <h1>Datos Representante.</h1>
        <br><br>
        <?= $form->field($model, 'name')->textInput(['placeholder' => $model->getAttributeLabel('name')])?>
        <br>
        <?= $form->field($model, 'lastname')->textInput(['placeholder' => $model->getAttributeLabel('lastname')])?>
        <br>
        <?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')])?>
        <br>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])?>
        <br>
        <?= $form->field($model, 'verif')->passwordInput(['placeholder' => $model->getAttributeLabel('verif')])?>
        <br />
        <p>Manifiesto que he leído y al hacer click en registrar, acepto la política de privacidad y protección de datos personales adoptada por el Instituto Colombiano Agropecuario – ICA y publicada para consulta en la página web <a href="https://www.ica.gov.co" target="_blank">https://www.ica.gov.co </a> Para lo cual autorizo a que el ICA pueda tratar datos personales conforme dicha política y en los términos en que ello sea necesario.</p>
        <br />
        <?= Html::submitButton("Guardar.", ["class" => "btn btn-primary"]) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
