<?php
/* @var $model app\models\forms\inhabilitarusuempForm */
/* @var $dataProvider \yii\data\ActiveDataProvider */
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

$url =  Url::to(['prueba/cambioestadousuemp'], 'http');
$javascript = <<<JS
function change(check)
{
    var keys = $('#usuarios').yiiGridView('getSelectedRows');
    if(keys.length>3)
        {
            event.preventDefault();
            alert('No puede habilitar mas de 3 usuarios');
            return;
        }
    
    $.ajax({
        url : '$url',
        data : { id : $(check).val() },
        type : 'POST',
        dataType : 'json',
        
        success : function(json){
            if(json.status=='error')
                {
                    $("input[value='"+json.value+"']").prop("checked","true");
                    alert(json.message);
                }else{
                    alert('Se realizo el cambio de estado');
                }
        }
    })
}
JS;

$this->registerJs($javascript,View::POS_END,'change');


?>
<h1></h1>
<?= GridView::widget([
    'id' => 'usuarios',
        'dataProvider' => $dataProvider,
        'columns' =>[
            [
                'attribute'=>'name',
                'header' => 'Nombre'
            ],
            [
                'attribute'=>'lastname',
                'header' => 'Apellido'
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'header'=>'Habilitado',
                'checkboxOptions' => function ($model) {
                    return ['onClick'=>'change(this)','checked' => ($model->userCompany->Estado_usuario==1)?true:false];
                }

            ],
        ]
]);
?>