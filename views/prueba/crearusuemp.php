<?php

/* @var $model app\models\forms\crearusuempForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;

if($registro_otro)
{
$script = <<< JS
     $("#confirma-otro").modal("show");
JS;
$this->registerJs($script, View::POS_END);    
}
?>

<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<main class="one">
    <div></div>
    
 <div>
     
     <h1><i class="fa fa-users" aria-hidden="true"></i></h1>
  
     <h2>Registro funcionarios delegados</h2> 
     <br>
<?php $form = ActiveForm::begin(['id' => 'crear-form']); ?>
<?= $form->errorSummary($model)?>

    <?= $form->field($model, 'name')->textInput(['maxlength'=>60,'placeholder'=>'Nombre del funcionario']) ?>
     <?= $form->field($model, 'lastname')->textInput(['maxlength'=>60,'placeholder'=>'Apellido del funcionario']) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength'=>60,'autofocus' => true,'placeholder'=>'Correo institucional']) ?>
  
<?= Html::submitButton('Guardar', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>

     <br>     
     <?php ActiveForm::end(); ?>
     <br>

     
</div>
    
</main>

<?php 
Modal::begin([
    'id'=>'confirma-otro',
    'header' => 'Confirmación',   
    'footer'=> Html::button("Aceptar",["class"=>"btn btn-success","data-dismiss"=>"modal"])." ".Html::a("Continuar","/siria/web/site/index",["class"=>"btn btn-info",])
]);

echo '¿Desea guardar otro usuario?';

Modal::end();



?>
