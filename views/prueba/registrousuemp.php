<?php

/* @var $model app\models\forms\registrousuemp */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<main class="one">
    <h1><i class="fa fa-user" aria-hidden="true"></i></h1>

    <div>

        <fieldset>
            <?php $form = ActiveForm::begin(['id' => 'registrousuemp-form',
                'fieldConfig' => ['errorOptions' => ['class' => 'help-block', 'encode' => false]]
            ]); ?>
            <?= $form->errorSummary($model)?>
            <?= $form->field($model, 'name')->textInput(['maxlength'=>60,'placeholder'=>'Nombre del usuario']) ?>
            <?= $form->field($model, 'lastname')->textInput(['maxlength'=>30,'placeholder'=>'Apellidos del usuario']) ?>
            <?= $form->field($model, 'username')->textInput(['maxlength'=>30,'placeholder'=>'Correo del usuario']) ?>
            <?= $form->field($model, 'password')->passwordInput(['maxlength'=>15,'placeholder'=>'Contraseña']) ?>
            <?= $form->field($model, 'confirmar')->passwordInput(['maxlength'=>15,'placeholder'=>'Confirmar contraseña']) ?>
            <?= $form->field($model, 'random')->textInput(['maxlength'=>11,'placeholder'=>'Codigo de verificacion']) ?>
            <fieldset/>
            <p>Manifiesto que he leído y al hacer click en registrar, acepto la política de privacidad y protección de datos personales adoptada por el Instituto Colombiano Agropecuario – ICA y publicada para consulta en la página web <a href="https://www.ica.gov.co" target="_blank"> https://www.ica.gov.co.</a> Para lo cual autorizo a que el ICA pueda tratar mis datos personales conforme dicha política y en los términos en que ello sea necesario.</p>
            <?= Html::submitButton('Registrar', ['class'=>'btn btn-success','name' => 'login-button']) ?>
            <?php ActiveForm::end(); ?>

    </div>

</main>
