<?php
/* @var $model app\models\forms\Formdelegadoica  */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;

if($supuesto){
    $script = <<< JS
        $("#Correcto").modal("show");
JS;
    $this->registerJs($script, View::POS_END);
}
?>
<div>
    <h1>Registro Apoyo ICA.</h1>
    <?php $form = ActiveForm::begin(['method'=>'POST']);?>
    <div class="input-group mb-3">
        <table class="table">
            <tr>
                <th><?= $form->field($model, 'name')->textInput(['placeholder' => $model->getAttributeLabel('name')])?></th>
                <th><?= $form->field($model, 'lastname')->textInput(['placeholder' => $model->getAttributeLabel('lastname')])?></th>
            </tr>
            <tr>
                <th><?= $form->field($model, 'dominio')->textInput(['placeholder' => $model->getAttributeLabel('dominio')])?></th>
                <th><?= $form->field($model, 'correo')->textInput(['disabled'=>'disabled'])?></th>
            </tr>
        </table>
        <br>
        <?= Html::submitButton("Guardar.", ["class" => "btn btn-primary"]) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
<?php
Modal::begin([
    'id'=>'Correcto',
    'header' => 'Desea inscribir otro usuario.'
]);
if($supuesto):
?>
    <div>
        <div>
            <label>¿Desea inscribir otro usuario apoyo?.</label><br /><br />
            <label> </label>
        </div><br /><br /><br />
        <div align='right'>
            <!--La Url aparecia en get por la variable que estaba enviando desde la linea de encima-->
            <button class='btn btn-primary btn-info' data-dismiss="modal">Si.</button>
            <?= Html::a('No.',['/site/index'],['class'=>'btn btn-primary btn-success'])?>
        </div>
    </div>
<?php
endif;
Modal::end();
?>