<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<h1>Tiempo extra para las empresas</h1>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
 <div class="input-group mb-3">
     <table class="table">
         <tr>
             <th>
             <th><?= $form->field($model, 'nit')->textInput(['placeholder' => $model->getAttributeLabel('nit')])?></th>
             </th>
             <th><?= $form->field($model, 'razon')->textInput(['placeholder' => $model->getAttributeLabel('razon')])?></th>
             <th>
                 <?= $form->field($model, 'fechainicio')->widget(\yii\jui\DatePicker::class,['options' => ['class' => 'form-control'],'dateFormat' => 'yyyy-MM-dd'])?>
             </th>
             <th>
                 <?= $form->field($model, 'fechafin')->widget(\yii\jui\DatePicker::class,['options' => ['class' => 'form-control'],'dateFormat' => 'yyyy-MM-dd'])?>
             </th>
         </tr>
     </table>
    </div>
    <th>
    <?= Html::submitButton("Guardar", ["class" => "btn btn-success"]) ?>
    </th>

    <?php $form->end() ?>
