<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<h1>Establecer fechas</h1>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
 <div class="input-group mb-3">
     <table class="table">
         <tr>
             <th>
                 <?= $form->field($model, 'fechainicio')->widget(\yii\jui\DatePicker::class,['options' => ['class' => 'form-control','placeholder' => $model->getAttributeLabel('fechainicio')],'dateFormat' => 'yyyy-MM-dd'])?>
             </th>
             <th>
                 <?= $form->field($model, 'fechafin')->widget(\yii\jui\DatePicker::class,['options' => ['class' => 'form-control','placeholder' => $model->getAttributeLabel('fechafin')],'dateFormat' => 'yyyy-MM-dd'])?>
             </th>
         </tr>
     </table>
</div>
<?= Html::submitButton("Guardar.", ["class" => "btn btn-success"]) ?>

<?php $form->end() ?>
