<?php

return [
    'adminEmail' => 'jonathan.cruz@ica.gov.co',
    'senderEmail' => 'jonathan.cruz@ica.gov.co',
    'senderName' => 'jonathan.cruz@ica.gov.co',
    'ProfileExclude'=>['Publico'=>'?','Logueado'=>'@'],
    'descripcion'=>'Sistema de información para el reporte de plaguicidas, bioinsumos y fertilizantes comercializados en el país',
    'disableLdap'=>true,
    'WSLdap'=>'http://www1.ica.gov.co/WebServices/LdapServices/UsersList.asmx?WSDL',
    'LdapServer'=>'ica.gov.co',//10.0.0.2,
    'UrlToken'=>'http://pruebasruesapi.rues.org.co/Token',
    'UrlConsultaNit'=>'http://pruebasruesapi.rues.org.co/api/consultasRUES/ConsultaNIT',
    'RUESAccessInfo'=>['Username'=>'ICAPruebas','Password'=>'R4qX3D-Z','grant_type'=>'password'],
    'profilesDisabled'=>['Representante legal'],
];
