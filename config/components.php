<?php
return [
    'queue' => [
        'class' => \yii\queue\db\Queue::class,
        'db' => 'db', // DB connection component or its config
        'tableName' => '{{%queue}}', // Table name
        'channel' => 'default', // Queue channel key
        'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
    ],
    'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => '2ydHEAPUDNIBEdO9Bv2htADv-QU6C1pT',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'user' => [
        'identityClass' => 'app\models\entities\Users',
        'enableAutoLogin' => false,
        'authTimeout' => 9000 //segundos de sesion
    ],
    'errorHandler' => [
        'errorAction' => 'site/error',
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'transport' => [
            'class'=>'Swift_SmtpTransport',
            'host'=>'smtp.office365.com',
            'username'=>'Notificaciones@ica.gov.co',
            'password'=>'X5FQbEcuK6vFV8sM:_/_',
            'port'=>'587',
            'encryption'=>'tls',
            'timeout'=>'60',
        ],
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'assignmentTable'=>'authassignment',
        'itemChildTable' => 'authitemchilds',
        'itemTable' => 'authitems',
        'ruleTable' => 'rules',
        // uncomment if you want to cache RBAC items hierarchy
//        'cache' => 'cache',
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
        ],
    ],
    'db' => $db,
];