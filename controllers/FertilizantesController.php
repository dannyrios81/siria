<?php
namespace app\controllers;

use app\components\BaseController;
use app\models\modelqueue;
use app\models\DescargueForm;
use Yii;

class FertilizantesController extends BaseController
{
    public function actionExportacionfer()
    {
        \Yii::$app->session->set('tipo',2);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('exportacionfer',['model'=>$model]);
    }

    public function actionImportacionfer()
    {
        \Yii::$app->session->set('tipo',3);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('importacionfer',['model'=>$model]);
    }

    public function actionProduccionfer()
    {
        \Yii::$app->session->set('tipo',1);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('produccionfer',['model'=>$model]);
    }
}
