<?php

namespace app\controllers;

use app\components\BaseController;
use app\jobs\BioExpo;
use app\jobs\BioImpo;
use app\jobs\BioProd;
use app\jobs\FertExpo;
use app\jobs\FertImpo;
use app\jobs\FertProd;
use app\jobs\PlagExpo;
use app\jobs\PlagImpo;
use app\jobs\PlagProd;
use app\models\carguearchivo;
use app\models\entities\Adjuntos;
use app\models\entities\Asignaciones;
use app\models\entities\Company;
use app\models\entities\ExpoBioinsumos;
use app\models\entities\Tipoadjunto;
use app\models\entities\UserCompany;
use app\models\entities\Fechas;
//use app\models\entities\UserCompany;
use app\models\modelqueue;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use Yii;

class CargarController extends BaseController
{
    public function actionCargar()
    {
        $fecha = Fechas::find()->where('id = 1')->one();
        $actual = date('Y-m-d');
        if($fecha['fecha_inicio'] <= $actual && $fecha['fecha_fin'] >= $actual)
        {
        $tipo = Yii::$app->session->get('tipo');
        $model = new carguearchivo();
        $iduser = Yii::$app->user->identity->getId();
        $idcompany = UserCompany::find()->where(['id_user' => $iduser])->one();
	        if (Yii::$app->request->isPost)
	        {
	            $model->file = UploadedFile::getInstance($model, 'file');
	            if ($model->file && $model->validate())
	            {
	                $nombre = str_replace('.','',microtime(true));
	                $extension = $model->file->extension;
                    $ext = ucfirst(trim($extension));
	                $nombrearchivo = $nombre . '.' . $extension;
	                $inputfile = Yii::getAlias('@xlsupload' . '/' . $idcompany['id_company'] . '/' . $nombrearchivo);
	                $sql = new Adjuntos();
	                $sql->nombre = $nombre;
	                $sql->extension =$extension;
	                $sql->id_company = $idcompany['id_company'];
	                $sql->id_adjunto = $tipo;
	                $sql->id_user = $iduser;
	                $sql->path = $inputfile;
	                $sql->save();
	                $categoria = Tipoadjunto::find()->where(['id_adjunto'=>$tipo])->one();
                    $company = Company::find()->where(['idcompany'=>$idcompany['id_company']])->one();
	                $asignaciones = new Asignaciones();
                    $asignaciones->idcompany = $idcompany['id_company'];
                    $asignaciones->nit = $company['nit'];
                    $asignaciones->name = $company['name'];
                    $asignaciones->idusers = $iduser;
                    $asignaciones->categoria = $categoria['nombreadjunto'];
                    $asignaciones->archivo = $nombre;
                    $asignaciones->estado = 'Registrado';
                    $asignaciones->asignado = 0;
                    VarDumper::dump($asignaciones,10,1);exit;
                    $asignaciones->save();
	                if ($model->file->saveAs($inputfile)) {
	                    Yii::$app->session->setFlash('success', 'Guardado');
                        if($tipo === 1){
                            Yii::$app->queue->delay(10)->push(new FertProd(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 2){
                            Yii::$app->queue->delay(10)->push(new FertExpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 3){
                            Yii::$app->queue->delay(10)->push(new FertImpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 4){
                            Yii::$app->queue->delay(10)->push(new BioProd(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 5){
                            Yii::$app->queue->delay(10)->push(new BioExpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 6){
                            Yii::$app->queue->delay(10)->push(new BioImpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 7){
                            Yii::$app->queue->delay(10)->push(new PlagProd(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 8){
                            Yii::$app->queue->delay(10)->push(new PlagExpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }elseif($tipo === 9){
                            Yii::$app->queue->delay(10)->push(new PlagImpo(['ext'=>$ext,'inputfile'=>$inputfile,'idcompany'=>$idcompany,'iduser'=>$iduser,'nombre'=>$nombre]));
                        }
	                } else {
	                    Yii::$app->session->setFlash('error', 'No guardado');
	                }
	            }
	        }
	        return $this->render('cargar',['model'=>$model]);
	    }
        else
        {
            Yii::$app->session->setFlash('error','La fecha inicial para el cargue de informacion '.$fecha['fecha_inicio'].' y la ultima fecha '.$fecha['fecha_fin'].' se encuentran expiradas');
            return $this->goHome();
        }


    }

    public function actionGuardar()
    {
        return $this->render('guardar');
    }

}