<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\forms\Establecerfechas;
use Yii;

class FechasController extends BaseController
{

    public function actionEstablecerfechas()
    {
        $model = new Establecerfechas();
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                if($model->save())
                {
                    Yii::$app->session->setFlash('success','Fechas Guardadas correctamente.');
                    return $this->goHome();
                }
            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render("establecerfechas", ["model" => $model]);
    }

    public function actionTiempoempresa(){
        $model = new Establecerfechas();
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
               if($model->addtime()){

               }
            }

        }
        return $this->render('tiempoempresa',['model'=> $model]);
    }


}
