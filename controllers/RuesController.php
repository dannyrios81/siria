<?php
namespace app\controllers;

use app\models\editarinfoForm;
use app\models\entities\Company;
use Yii;
use app\components\BaseController;
use app\models\rues;
use app\models\editarinfo;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use app\models\empresa;

class RuesController extends BaseController {



    public function actionEnviarDatos()
    {
        $rtaRUES[] = $this->actionCrearempresa();
        $this->actionEmpresa($rtaRUES[]);
    }

    public function actionCrearempresa() {
        //si esta logueado lo redirige al home del proyecto.
        if(!Yii::$app->user->isGuest)
            return $this->goHome();

        $Modato = false;
        $model = new rues();
        $respuesta=[];
        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            $ClienteRues = new Client();
            $responseToken = $ClienteRues->createRequest()->setMethod('POST')->setUrl(\Yii::$app->params['UrlToken'])->setData(\Yii::$app->params['RUESAccessInfo'])->send();
            if ($responseToken->isOk) {
                $data = \Yii::$app->params['UrlConsultaNit'] . "?usuario=" . \Yii::$app->params['RUESAccessInfo']['Username'] . "&nit=" . $model->campo . "&dv=". $model->verif;
                $token = $responseToken->data['token_type'] . " " . $responseToken->data['access_token'];
                $respuestaNit = $ClienteRues->createRequest()->setMethod('POST')->setUrl($data)->setHeaders(['Authorization' => $token])->send();
                if (!isset($respuestaNit->data['error']))
                {
//                    VarDumper::dump($respuestaNit,10,1);exit;//TODO *imprime todos los Datos que trae RUES*
                    foreach ($respuestaNit->getData()['registros'] as $value) {
                        $respuesta['razon_social'] = $value['razon_social'];
                        $respuesta['tipo_identificacion'] = $value['tipo_identificacion'];
                        $respuesta['numero_identificacion'] = $value['numero_identificacion'];
                        $respuesta['estado_matricula'] = $value['estado_matricula'];
                        $respuesta['matricula'][] = $value['matricula'];
                        if(isset($value['establecimientos']) and is_array(isset($value['establecimientos'])) and count($value['establecimientos'])>0)
                        {
                            $respuesta['correo_electronico_fiscal'] = $value['establecimientos'][0]['correo_electronico_fiscal'];
                            $respuesta['direccion_fiscal'] = $value['establecimientos'][0]['direccion_fiscal'];
                        }
                        if(isset($value['telefono_fiscal_1']))
                        {
                            $respuesta['telefono_fiscal_1'] = $value['telefono_fiscal_1'];
                        }
                        $estado = 'CANCELADA';
                        if($respuesta['estado_matricula'] != $estado){
                            if (!is_null($respuesta['matricula']) and !empty($respuesta['matricula']))
                            {
                                $Modato = true;
                            }else{
                                $Modato = false;
                            }
                        }
                        else
                        {
                            Yii::$app->session->setFlash('error', "La matricula mercantil para esta empresa se encuentra Cancelada.");
                        }
                    }
                    Yii::$app->session->set('infoRues', $respuesta);
                }
                else
                {
                    Yii::$app->session->setFlash('error', "Numero Único Tributario no retorna busqueda.");
                }
            }else{
                echo "error (" . $responseToken->data['error_description'] . ")";
            }
        }
        return $this->render('crearempresa', ['model' => $model, 'supuesto'=>$Modato, 'data'=>$respuesta]);
    }

    public function actionEmpresa(){
        $model = new empresa();
        if ($model->load(Yii::$app->request->post()) and $model->validate()){
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);
                if($model->save()){
                    Yii::$app->session->setFlash('success', "Empresa y Representante legal Creados Correctamente, Para el ingreso al aplicativo, Use su correo ");
                    return $this->redirect(["site/index"]);
                }
        }
        return $this->render('empresa',['model'=>$model]);
    }


    public function actionEditarinfo() {
        $model = new editarinfoForm();
        switch (Yii::$app->request->post('dato'))
        {
            case "A":
                $datosA = Company::find()->select('name, nit, phone, address, email ')->where('nit = :nit')->params([':nit'=>Yii::$app->request->post('buscar')])->one();
                VarDumper::dump($datosA,10,1);
                break;
            case "B":
                $datosB = Company::find()->select('name, nit, phone, address, email ')->where(['name = :name'])->params([':name'=> $model->combo])->all();
                break;
            default:
                break;
        }
        return $this->render('editarinfo',['model'=>$model]);
    }
}