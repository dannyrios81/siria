<?php

namespace app\controllers;

use app\models\entities\Company;
use app\models\entities\Users;
use Yii;
use app\components\BaseController;
use app\models\forms\Formdelegadoica;
use yii\helpers\VarDumper;

class RegistrosController extends BaseController
{

    public function actionDelegadoica()
    {
        $modal = false;
        $model = new Formdelegadoica();
        if ($model->load(Yii::$app->request->post()) and $model->validate()){
//            if($model->save())
//            {
                Yii::$app->mailer->compose()
                    ->setFrom('Notificaciones@ica.gov.co')
                    ->setTo($model->dominio.'@ica.gov.co')
                    ->setSubject('Asignación usuario en SIRIA')
                    ->setHtmlBody('<html><head><h1>Asignación usuario en SIRIA</h1></head><body>Buen día.<br /><br />Apreciado Usuario.<br /><br />Usted fue seleccionado para colaborar con la revisión y control de los archivos enviados por parte de las empresas para su uso en el aplicativo SIRIA,<br /> Para ingresar al sistema, digite sus credenciales de dominio ICA en el siguiente enlace http://192.168.1.24/siria/web/<br />Ejemplo.<br /> Usuario: Nombre.apellido <br /> Contraseña: Mi clave de dominio<br /><br />Muchas gracias por su colaboración</body></html>')
                    ->send();
                Yii::$app->session->setFlash("success",'Usuario ICA para la funcion de APOYO creado correctamente.');
                $modal = true;
                $model = new Formdelegadoica();
//            }
        }
        return $this->render('delegadoica',['model'=>$model,'supuesto'=>$modal]);
    }
}