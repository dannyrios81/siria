<?php

namespace app\controllers;

use app\models\entities\Company;
use app\models\entities\UserCompany;
use app\models\entities\Users;
use Yii;
use app\models\forms\crearusuempForm;
use app\models\forms\inhabilitarusuempForm;
use app\models\forms\registrousuemp;
use app\models\forms\mistareasForm;
use app\components\BaseController;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\User;


class PruebaController extends BaseController

{
    public function actionCrearusuemp()
    {

        $usuario = UserCompany::find()->where("id_user = :idus")->params([':idus' => Yii::$app->user->identity->idusers])->one();
        Yii::$app->session->set("idcompania", $usuario->id_company);

        $model = new crearusuempForm();
        $boolean = false;


        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            if ($model->cantidadmaximausuarios() < 3 and $model->save()) {
                $boolean = true;
                $model = new crearusuempForm();
            } else {
                Yii::$app->session->setFlash('success', 'Ya se ingresaron la cantidad maxima de usuarios con exito!!');
                return $this->redirect(["/"]);
            }
        }

        return $this->render('crearusuemp', ['model' => $model, 'registro_otro' => $boolean,]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegistrousuemp()
    {
        $model = new registrousuemp();

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            $model->password = password_hash($model->password, PASSWORD_BCRYPT);

//            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
           if($model->save())
           {
               Yii::$app->mailer->compose()
                   ->setTo($model->username)
                   ->setFrom('Notificaciones@ica.gov.co')
                   ->setSubject('Usuario Registrado')
                   ->setHtmlBody('<html><body>Buen día.<br /><br />Apreciado Usuario.<br /><br />Su usuario a sido regisrtado correctamente, para el ingreso use el siguiente enlace http://192.168.1.24/siria/web/<br /><br />Muchas gracias por su colaboración</body></html>')
                   ->send();
                Yii::$app->session->setFlash("success",'Excelente');
                return $this->redirect(["site/index"]);
           }
       }
        return $this->render('registrousuemp', ['model' => $model]);
    }

    public function actionInhabilitarusuemp()
    {
        $UsuarioLogueado = Yii::$app->user->identity;
        $usuarios = Users::find()->joinWith(['userCompany', 'userCompany.company'])
            ->where(['user_company.id_company' => $UsuarioLogueado->userCompany->id_company]);

        $dataProvider = new ActiveDataProvider([
            'query' => $usuarios,
            'pagination' => [
                'pageSize' => 15
            ],
            'sort' => [
                'defaultOrder' => ['name' => SORT_ASC]
            ]
        ]);

        return $this->render('inhabilitarusuemp', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @todo Hacer la funcion de cambio para los estados de los usuarios
     */
        public function actionCambioestadousuemp(){
            if(Yii::$app->request->post('id') != Yii::$app->user->id)
            {
                $Usercompany = UserCompany::find()->where(['id_user'=>Yii::$app->request->post('id')])->one();
                $Usercompany->Estado_usuario = !$Usercompany->Estado_usuario?1:0;
                echo Json::encode(['status'=>'success','message'=>'','value'=>Yii::$app->request->post('id')]);
                $Usercompany->save();
            }
            else
            {
                echo Json::encode(['status'=>'error','message'=>'No se puede inhabilitar el usuario logueado','value'=>Yii::$app->request->post('id')]);
            }
        }

    /**
     * @todo funcion para mostrar como se se ejecuta la cortina del ica
     **/
    public function actionCortina()
    {
        return $this->render("cortina");
    }

    public function actionMistareas()
    {
        return $this->render('mistareas');
    }
}
