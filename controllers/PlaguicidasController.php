<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\DescargueForm;
use Yii;

class PlaguicidasController extends BaseController
{
    public function actionExportacionpla()
    {
        Yii::$app->session->set('tipo',8);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('exportacionpla',['model'=>$model]);
    }

    public function actionImportacionpla()
    {
        Yii::$app->session->set('tipo',9);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('importacionpla',['model'=>$model]);
    }

    public function actionProduccionpla()
    {
        Yii::$app->session->set('tipo',7);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('produccionpla',['model'=>$model]);
    }

}
