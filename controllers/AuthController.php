<?php

namespace app\controllers;

use app\components\BaseController;
use app\components\Utilities;
use Yii;
use app\models\entities\Users;
use app\models\searchModels\UsersSearch;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * AuthController implements the CRUD actions for Users model.
 */
class AuthController extends BaseController
{

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex($profile = '')
    {
        $searchModel = new UsersSearch();
//        VarDumper::dump(Yii::$app->request->queryParams,10,true);exit;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $profile);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAssignauth($id)
    {
        $model = $this->findModel($id);
        return $this->render('assignauth', ['model' => $model]);
    }

    /* acciones ajax */
    public function actionAssignrevoke($id)
    {
        $noredirect = !empty(Yii::$app->request->post('noredirect')) ? Yii::$app->request->post('noredirect'):false;
        if(!empty($_REQUEST['auth']))
        {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($_REQUEST['auth']);

            if($auth->checkAccess($id,$_REQUEST['auth'])) {
                $auth->revoke($role, $id);
            }
            else {
                $auth->assign($role, $id);
            }
        }
        if(!$noredirect)
        {
            $this->redirect(Url::to(['auth/assignauth','id'=>$id]));
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
