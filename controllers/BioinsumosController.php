<?php
namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\DescargueForm;

class BioinsumosController extends BaseController
{
    public function actionExportacionbio()
    {
        Yii::$app->session->set('tipo',5);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('exportacionbio',['model' => $model]);
    }

    public function actionImportacionbio()
    {
        Yii::$app->session->set('tipo',6);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
            break;
            default:
            break;
        }
        return $this->render('importacionbio',['model' => $model]);
    }

    public function actionProduccionbio()
    {
        Yii::$app->session->set('tipo',4);
        $model = new DescargueForm();
        switch (Yii::$app->request->post('enviar'))
        {
            case 'A':
                $model->descargar();
                break;
            default:
                break;
        }
        return $this->render('produccionbio',['model' => $model]);
    }
}
