<?php

use yii\db\Migration;

/**
 * Class m191101_152848_Prod_Bioinsumos
 */
class m191101_152848_Prod_Bioinsumos extends Migration
{
    public function up()
    {
        $this->createTable('Prod_Bioinsumos',[
        'id' => $this->primaryKey(),
        'regi_venta' => $this->integer(10)->notNull(),
        'nomb_comer' => $this->string(255)->notNull(),
        'ingr_activ' => $this->string(255)->notNull(),
        'clase' => $this->string()->notNull(),
        'Unid_Medid' => $this->string()->notNull(),
        'concentra' => $this->string()->notNull(),
        'tipo' => $this->string()->notNull(),
        'volu_prod' => $this->string()->notNull(),
        'volu_vent' => $this->string()->notNull(),
        'iduser' => $this->integer()->notNull(),
        'idcompany' => $this->integer()->notNull(),
        'idestado' => $this->integer()->notNull()
    ]);
        $this->addForeignKey('PB-U_iduser','Prod_Bioinsumos','iduser','users','idusers');
        $this->addForeignKey('PB-C_idcompany','Prod_Bioinsumos','idcompany','company','idcompany');
//        $this->addForeignKey('PB-E_idestado','Prod_Bioinsumos','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('PB-U_iduser','Prod_Bioinsumos');
        $this->dropForeignKey('PB-C_idcompany','Prod_Bioinsumos');
//        $this->dropForeignKey('PB-E_idestado','Prod_Bioinsumos');
        $this->dropTable('Prod_Bioinsumos');
    }
}