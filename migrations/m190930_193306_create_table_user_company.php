<?php

use yii\db\Migration;

/**
 * Class m190930_193306_create_table_user_company
 */
class m190930_193306_create_table_user_company extends Migration
{
    /**
     * {@inheritdoc}

    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}

    public function safeDown()
    {
        echo "m190930_193306_create_table_user_company cannot be reverted.\n";

        return false;
    }

    /*
     */  // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('user_company', [
            'id_user'=>  $this->integer()->notNull(),
            'id_company'=> $this->integer()->notNull(),
            'Estado_usuario'=> $this->integer()->notNull(),
            'Random'=>$this->string(11),
        ]);

    }

    public function down()
    {
        $this->dropTable('user_company');
    }

}
