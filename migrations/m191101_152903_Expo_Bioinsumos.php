<?php

use yii\db\Migration;

/**
 * Class m191101_152903_Expo_Bioinsumos
 */
class m191101_152903_Expo_Bioinsumos extends Migration
{
    public function up()
    {
        $this->createTable('Expo_Bioinsumos',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'ingr_activ' => $this->string(255)->notNull(),
            'clase' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'uso' => $this->string()->notNull(),
            'paisdestin' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('EB-U_iduser','Expo_Bioinsumos','iduser','users','idusers');
        $this->addForeignKey('EB-C_idcompany','Expo_Bioinsumos','idcompany','company','idcompany');
//        $this->addForeignKey('EB-E_idestado','Expo_Bioinsumos','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('EB-U_iduser','Expo_Bioinsumos');
        $this->dropForeignKey('EB-C_idcompany','Expo_Bioinsumos');
//        $this->dropForeignKey('EB-E_idestado','Expo_Bioinsumos');
        $this->dropTable('Expo_Bioinsumos');
    }
}