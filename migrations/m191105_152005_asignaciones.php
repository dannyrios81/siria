<?php

use yii\db\Migration;



class m191105_152005_asignaciones extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('asignaciones',[
            'id'=>$this->primaryKey(),
            'idcompany'=>$this->integer(15)->notNull(),
            'nit'=>$this->integer(15)->notNull(),
            'name'=>$this->string(255)->notNull(),
            'idusers'=>$this->integer()->null(),
            'categoria'=>$this->string()->notNull(),
            'subcategoria'=>$this->string()->notNull(),
            'fecha_cargue'=>$this->dateTime()->notNull(),
            'estado'=>$this->string()->notNull(),
            'asignado'=>$this->boolean()->notNull(),
        ]);
        $this->addForeignKey('asi_usu-id', 'asignaciones', 'idusers', 'users', 'idusers');
        $this->addForeignKey('asi_com-nom', 'asignaciones', 'idcompany', 'company', 'idcompany');
    }

    public function down()
    {
        $this->dropForeignKey('asi_usu-id','asignaciones');
        $this->dropForeignKey('asi_com-nom','asignaciones');
        $this->dropTable('asignaciones');
    }

}
