<?php

use yii\db\Migration;

/**
 * Class m191101_145825_Impo_Fertilizantes
 */
class m191101_145825_Impo_Fertilizantes extends Migration
{
    public function up()
    {
        $this->createTable('Import_Fertilizantes',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'Unid_Medid' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'clase' => $this->string()->notNull(),
            'uso' => $this->string()->notNull(),
            'paisorigen' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('IF-U_iduser','Import_Fertilizantes','iduser','users','idusers');
        $this->addForeignKey('IF-C_idcompany','Import_Fertilizantes','idcompany','company','idcompany');
//        $this->addForeignKey('IF-E_idestado','Import_Fertilizantes','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('IF-U_iduser','Import_Fertilizantes');
        $this->dropForeignKey('IF-C_idcompany','Import_Fertilizantes');
//        $this->dropForeignKey('IF-E_idestado','Import_Fertilizantes');
        $this->dropTable('Import_Fertilizantes');
    }
}