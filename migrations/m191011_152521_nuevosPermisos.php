<?php

use app\models\entities\AppSkeleton;
use app\models\entities\AppSkeletonHasAuthitems;
use yii\db\Migration;

/**
 * Class m191011_152521_nuevosPermisos
 */
class m191011_152521_nuevosPermisos extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $representanteLegal = $auth->createRole('Representante Legal');
        $representanteLegal->description='Representante Legal';

        $delegadoresponsable = $auth->createRole('Delegado Responsable');
        $delegadoresponsable->description='Delegado Responsable';

        $apoyo = $auth->createRole('Apoyo');
        $apoyo->description='Apoyo';

        //$auth->add($representanteLegal);
//        $auth->add($delegadoresponsable);
//        $auth->add($apoyo);

        $appskeleton = new AppSkeleton();
        $appskeleton->controllerid = 'prueba';
        $appskeleton->actionid = 'crearusuemp';
        $appskeleton->description = 'Registro funcionarios delegados';
        $appskeleton->levels = 2;
        $appskeleton->visible = 1;
        $appskeleton->save(false);

//        $appskeleton_authitem = new AppSkeletonHasAuthitems();
//        $appskeleton_authitem->auth_name = $representanteLegal->name;
//        $appskeleton_authitem->idapp_skeleton = $appskeleton->idapp_skeleton;
//        $appskeleton_authitem->save(false);

//        $appskeleton = new AppSkeleton();
//        $appskeleton->controllerid = 'prueba';
//        $appskeleton->actionid = 'crearusuemp';
//        $appskeleton->description = 'prueba de submenu';
//        $appskeleton->idapp_skeleton_parent = 14;
//        $appskeleton->levels = 3;
//        $appskeleton->visible = 1;
//        $appskeleton->save(false);
//
//        $appskeleton_authitem = new AppSkeletonHasAuthitems();
//        $appskeleton_authitem->auth_name = $representanteLegal->name;
//        $appskeleton_authitem->idapp_skeleton = $appskeleton->idapp_skeleton;
//        $appskeleton_authitem->save(false);


    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $rol = $auth->getRole('Representante Legal');
        $appskeleton = AppSkeleton::find()->where(['controllerid' => 'prueba','actionid' => 'crearusuemp'])->one();
        $appskeleton_authitem = AppSkeletonHasAuthitems::find()->where(['auth_name'=>$rol->name,'idapp_skeleton'=>$appskeleton->idapp_skeleton])->one();

        //\yii\helpers\VarDumper::dump($appskeleton_authitem,10,0);exit;

        $appskeleton_authitem->delete();
        $appskeleton->delete();

        $auth->remove($rol);

        $rol2 = $auth->getRole('Delegado Responsable');
        $appskeleton2 = AppSkeleton::find()->where(['description' => 'prueba de submenu'])->one();
        $appskeleton_authitem2 = AppSkeletonHasAuthitems::find()->where(['auth_name'=>$rol2->name,'idapp_skeleton'=>$appskeleton2->idapp_skeleton])->one();

        //\yii\helpers\VarDumper::dump($appskeleton_authitem,10,0);exit;

        $appskeleton_authitem2->delete();
        $appskeleton2->delete();

        $auth->remove($rol2);
    }
}
