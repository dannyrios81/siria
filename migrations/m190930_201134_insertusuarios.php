<?php

use yii\db\Migration;

/**
 * Class m190930_201134_insertusuarios
 */
class m190930_201134_insertusuarios extends Migration
{
    public function up()
    {
        $this->insert('authitems',['name'=>'Representante legal','type'=>1,'description'=>'Representante legal','created_at'=>time()]);
    }

    public function down()
    {
        $this->delete('authitems',['name'=>'Representante legal']);
    }
}
