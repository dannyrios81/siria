<?php

use yii\db\Migration;

/**
 * Class m190930_191923_create_table_company
 */
class m190930_191923_create_table_company extends Migration
{
    /**
     * {@inheritdoc}

    public function safeUp()
    {

    }


     * {@inheritdoc}

    public function safeDown()
    {
        echo "m190930_191923_create_table_company cannot be reverted.\n";

        return false;
    }


   */ // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('company', [
            'idcompany'=> $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'nit'=> $this->integer()->notNull(),
            'phone'=> $this->integer()->notNull(),
            'address'=> $this->string()->notNull(),
            'email'=> $this->string()->notNull(),




        ]);

    }

    public function down()
    {
        $this->dropTable('company');
    }

}
