<?php

use yii\db\Migration;

/**
 * Class m191101_140223_Prod_Plaguicidas
 */
class m191101_140223_Prod_Plaguicidas extends Migration
{
    public function up()
    {
        $this->createTable('Prod_Plaguicidas',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'ingr_activ' => $this->string(255)->notNull(),
            'clase' => $this->string()->notNull(),
            'Unid_Medid' => $this->string()->notNull(),
            'concentra' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'volu_prod' => $this->string()->notNull(),
            'volu_vent' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer(11)
        ]);
        $this->addForeignKey('PP-U_iduser','Prod_Plaguicidas','iduser','users','idusers');
        $this->addForeignKey('PP-C_idcompany','Prod_Plaguicidas','idcompany','company','idcompany');
//        $this->addForeignKey('PP-E_idestado','Prod_Plaguicidas','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('PP-U_iduser','Prod_Plaguicidas');
        $this->dropForeignKey('PP-C_idcompany','Prod_Plaguicidas');
//        $this->dropForeignKey('PP-E_idestado','Prod_Plaguicidas');
        $this->dropTable('Prod_Plaguicidas');
    }
}
