<?php

use yii\db\Migration;

/**
 * Class m191101_145835_Prod_Fertilizantes
 */
class m191101_145835_Prod_Fertilizantes extends Migration
{
    public function up()
    {
        $this->createTable('Prod_Fertilizantes',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'tipo' => $this->string()->notNull(),
            'Unid_Medid' => $this->string()->notNull(),
            'produccion' => $this->string(255)->notNull(),
            'volu_vent' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('PF-U_iduser','Prod_Fertilizantes','iduser','users','idusers');
        $this->addForeignKey('PF-C_idcompany','Prod_Fertilizantes','idcompany','company','idcompany');
//        $this->addForeignKey('PF-E_idestado','Prod_Fertilizantes','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('PF-U_iduser','Prod_Fertilizantes');
        $this->dropForeignKey('PF-C_idcompany','Prod_Fertilizantes');
//        $this->dropForeignKey('PF-E_idestado','Prod_Fertilizantes');
        $this->dropTable('Prod_Fertilizantes');
    }
}