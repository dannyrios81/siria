<?php

use yii\db\Migration;

/**
 * Class m191101_140305_Impo_Plaguicidas
 */
class m191101_140305_Impo_Plaguicidas extends Migration
{
    public function up()
    {
        $this->createTable('Import_Plaguicidas',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'ingr_activ' => $this->string(255)->notNull(),
            'concentra' => $this->string()->notNull(),
            'clase' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'volumen' => $this->string()->notNull(),
            'paisorigen' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('IP-U_iduser','Import_Plaguicidas','iduser','users','idusers');
        $this->addForeignKey('IP-C_idcompany','Import_Plaguicidas','idcompany','company','idcompany');
//        $this->addForeignKey('IP-E_idestado','Import_Plaguicidas','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('IP-U_iduser','Import_Plaguicidas');
        $this->dropForeignKey('IP-C_idcompany','Import_Plaguicidas');
//        $this->dropForeignKey('IP-E_idestado','Import_Plaguicidas');
        $this->dropTable('Import_Plaguicidas');
    }
}