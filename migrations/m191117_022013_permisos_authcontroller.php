<?php

use app\models\entities\AppSkeleton;
use app\models\entities\AppSkeletonHasAuthitems;
use yii\db\Migration;

/**
 * Class m191117_022013_permisos_authcontroller
 */
class m191117_022013_permisos_authcontroller extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $site = [
            ['controllerid'=>'auth','actionid'=>'index','description'=>'Seguridad y Perfiles','levels'=>2,'visible'=>1,'Perfiles'=>['Administrador']],
            ['controllerid'=>'auth','actionid'=>'assignauth','description'=>'asignacion autorizacion','levels'=>2,'visible'=>0,'Perfiles'=>['Administrador']],
            ['controllerid'=>'auth','actionid'=>'assignrevoke','description'=>'revocacion autorizacion','levels'=>2,'visible'=>0,'Perfiles'=>['Administrador']],
        ];

        foreach ($site as $item) {
            $appskeleton = new AppSkeleton();
            $appskeleton->load($item,'');
            $appskeleton->save();
            foreach ($item['Perfiles'] as $perfile) {
                $appskeletonhasauthitem = new AppSkeletonHasAuthitems(['idapp_skeleton'=>$appskeleton->idapp_skeleton,'auth_name' => $perfile]);
                $appskeletonhasauthitem->save();
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191117_022013_permisos_authcontroller cannot be reverted.\n";

        return false;
    }

}
