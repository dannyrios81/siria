<?php

use yii\db\Migration;

/**
 * Class m191101_145815_Expo_Fertilizantes
 */
class m191101_145815_Expo_Fertilizantes extends Migration
{
    public function up()
    {
        $this->createTable('Export_Fertilizantes',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'Unid_Medid' => $this->string(255)->notNull(),
            'tipo' => $this->string()->notNull(),
            'clase' => $this->string()->notNull(),
            'uso' => $this->string()->notNull(),
            'paisdestin' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('EF-U_iduser','Export_Fertilizantes','iduser','users','idusers');
        $this->addForeignKey('EF-C_idcompany','Export_Fertilizantes','idcompany','company','idcompany');
//        $this->addForeignKey('EF-E_idestado','Export_Fertilizantes','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('EF-U_iduser','Export_Fertilizantes');
        $this->dropForeignKey('EF-C_idcompany','Export_Fertilizantes');
//        $this->dropForeignKey('EF-E_idestado','Export_Fertilizantes');
        $this->dropTable('Export_Fertilizantes');
    }
}