<?php

use yii\db\Migration;

/**
 * Class m191101_152856_Impo_Bioinsumos
 */
class m191101_152856_Impo_Bioinsumos extends Migration
{
    public function up()
    {
        $this->createTable('Impo_Bioinsumos',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'ingr_activ' => $this->string(255)->notNull(),
            'clase' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'uso' => $this->string()->notNull(),
            'paisorigen' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('IB-U_iduser','Impo_Bioinsumos','iduser','users','idusers');
        $this->addForeignKey('IB-C_idcompany','Impo_Bioinsumos','idcompany','company','idcompany');
//        $this->addForeignKey('IB-E_idestado','Impo_Bioinsumos','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('IB-U_iduser','Impo_Bioinsumos');
        $this->dropForeignKey('IB-C_idcompany','Impo_Bioinsumos');
//        $this->dropForeignKey('IB-E_idestado','Impo_Bioinsumos');
        $this->dropTable('Impo_Bioinsumos');
    }
}