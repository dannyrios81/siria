<?php

use yii\db\Migration;

/**
 * Class m190930_203841_llave
 */
class m190930_203841_llave extends Migration
{
    public function up()
    {
        $this->addForeignKey('fork1', 'user_company', 'id_user', 'users', 'idusers');
        $this->addForeignKey('fork2', 'user_company', 'id_company', 'company', 'idcompany');
    }

    public function down()
    {
        $this->dropForeignKey('fork1', 'user_company');
        $this->dropForeignKey('fork2', 'user_company');
    }
}
