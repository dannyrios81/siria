<?php

use yii\db\Migration;

/**
 * Class m191101_140320_Expo_Plaguicidas
 */
class m191101_140320_Expo_Plaguicidas extends Migration
{
    public function up()
    {
        $this->createTable('Export_Plaguicidas',[
            'id' => $this->primaryKey(),
            'regi_venta' => $this->integer(10)->notNull(),
            'nomb_comer' => $this->string(255)->notNull(),
            'ingr_activ' => $this->string(255)->notNull(),
            'concentra' => $this->string()->notNull(),
            'clase' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'volumen' => $this->string()->notNull(),
            'paisdestin' => $this->string()->notNull(),
            'iduser' => $this->integer()->notNull(),
            'idcompany' => $this->integer()->notNull(),
            'idestado' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('EP-U_iduser','Export_Plaguicidas','iduser','users','idusers');
        $this->addForeignKey('EP-C_idcompany','Export_Plaguicidas','idcompany','company','idcompany');
//        $this->addForeignKey('EP-E_idestado','Export_Plaguicidas','idestado','Estado','idestado');
    }

    public function down()
    {
        $this->dropForeignKey('EP-U_iduser','Export_Plaguicidas');
        $this->dropForeignKey('EP-C_idcompany','Export_Plaguicidas');
//        $this->dropForeignKey('EP-E_idestado','Export_Plaguicidas');
        $this->dropTable('Export_Plaguicidas');
    }
}
