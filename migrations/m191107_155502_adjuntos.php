<?php

use yii\db\Migration;

class m191107_155502_adjuntos extends Migration
{
    public function up()
    {
        $this->createTable('tipoadjunto',[
            'id_adjunto'=> $this->primaryKey(),
            'nombreadjunto'=>$this->string(255)
        ]);
        $this->createTable('adjuntos',[
            'id' =>$this->primaryKey(),
            'nombre'=>$this->integer(255)->notNull(),
            'extension'=>$this->string(4)->notNull(),
            'id_company'=>$this->integer()->notNull(),
            'id_adjunto' =>$this->integer()->notNull(),
            'id_user'=>$this->integer()->notNull(),
            'path'=>$this->string(255)->notNull(),
        ]);
        $this->addForeignKey('tiad_adju-id','adjuntos','id_adjunto','tipoadjunto','id_adjunto');
        $this->addForeignKey('comp_adju-id','adjuntos','id_company','company','idcompany');
        $this->addForeignKey('user_adju-id','adjuntos','id_user','users','idusers');
    }
    public function down()
    {
        $this->dropForeignKey('tiad_adju-id','adjuntos');
        $this->dropForeignKey('comp_adju-id','adjuntos');
        $this->dropForeignKey('user_adju-id','adjuntos');
        $this->dropTable('tipoadjunto');
        $this->dropTable('adjuntos');
    }
}