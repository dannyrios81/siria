<?php

use yii\db\Migration;

/**
 * Class m190828_020541_skeleton_tables
 */
class m190828_020541_skeleton_tables extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('app_skeleton',[
            'idapp_skeleton'=>$this->primaryKey(),
            'controllerid'=>$this->string(100),
            'actionid'=>$this->string(100),
            'description'=>$this->string(100),
            'idapp_skeleton_parent'=>$this->integer(11),
            'levels'=>$this->integer(11),
            'visible'=>$this->integer(1)
        ],'DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci');

        $this->createTable('app_skeleton_has_authitems',[
            'idapp_skeleton'=>$this->integer(11),
            'auth_name'=>$this->string(64)->notNull()
        ],'DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci');

        $this->addPrimaryKey('app_skeleton_has_authitems_PK','app_skeleton_has_authitems',['idapp_skeleton','auth_name']);
        $this->createIndex('IN_appskeletonhasauthitems_authname','app_skeleton_has_authitems','auth_name');
//
        $this->addForeignKey('FK_app_skeleton_idapp_skeleton_parent','app_skeleton','idapp_skeleton_parent','app_skeleton','idapp_skeleton');
        $this->addForeignKey('FK_appskeletonhasauthitems_idappskeleton','app_skeleton_has_authitems','idapp_skeleton','app_skeleton','idapp_skeleton');
        $this->addForeignKey('FK_appskeletonhasauthitems_authname','app_skeleton_has_authitems','auth_name','authitems','name');
    }

    public function down()
    {
        $this->dropForeignKey('FK_app_skeleton_idapp_skeleton_parent','app_skeleton');
        $this->dropForeignKey('FK_appskeletonhasauthitems_idappskeleton','app_skeleton_has_authitems');
        $this->dropForeignKey('FK_appskeletonhasauthitems_authname','app_skeleton_has_authitems');

        $this->dropTable('app_skeleton');
        $this->dropTable('app_skeleton_has_authitems');

    }
}
