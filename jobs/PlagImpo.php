<?php

namespace app\jobs;

use app\models\entities\ExpoBioinsumos;
use app\models\entities\ImportPlaguicidas;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class PlagImpo.
 */
class PlagImpo extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public $nombre;
    public $ext;
    public $inputfile;
    public $idcompany;
    public $iduser;

    public function execute($queue)
    {
        $reader = IOFactory::createReader($this->ext);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->inputfile);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray(null,true,true,false);
        for($row=7;$row<count($sheetdata);$row++)
        {
            if ($row >= 7)
            {
                $datos = new ImportPlaguicidas();
                $datos->setIsNewRecord(true);
                $datos->regi_venta = $sheetdata[$row][0];
                $datos->nomb_comer = $sheetdata[$row][1];
                $datos->ingr_activ = $sheetdata[$row][2];
                $datos->concentra = $sheetdata[$row][3];
                $datos->clase = $sheetdata[$row][4];
                $datos->tipo = $sheetdata[$row][5];
                $datos->volumen = $sheetdata[$row][6];
                $datos->paisorigen = $sheetdata[$row][6];
                $datos->iduser = $this->iduser;
                $datos->idcompany = $this->idcompany['id_company'];
                $datos->idestado = 1;
                $datos->nombre = $this->nombre;
                $datos->save();
            }
        }
    }
}
