<?php

namespace app\jobs;

use app\models\entities\ExpoBioinsumos;
use app\models\entities\ProdBioinsumos;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class BioProd.
 */
class BioProd extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public $nombre;
    public $ext;
    public $inputfile;
    public $idcompany;
    public $iduser;

    public function execute($queue)
    {
        $reader = IOFactory::createReader($this->ext);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->inputfile);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray(null,true,true,false);
        for($row=7;$row<count($sheetdata);$row++)
        {
            if ($row >= 7)
            {
                $datos = new ProdBioinsumos();
                $datos->setIsNewRecord(true);
                $datos->regi_venta = $sheetdata[$row][0];
                $datos->nomb_comer = $sheetdata[$row][1];
                $datos->ingr_activ = $sheetdata[$row][2];
                $datos->clase = $sheetdata[$row][3];
                $datos->Unid_Medid = $sheetdata[$row][4];
                $datos->concentra = $sheetdata[$row][5];
                $datos->volu_prod = $sheetdata[$row][6];
                $datos->volu_vent = $sheetdata[$row][7];
                $datos->iduser = $this->iduser;
                $datos->idcompany = $this->idcompany['id_company'];
                $datos->idestado = 1;
                $datos->nombre = $this->nombre;
                $datos->save();
            }
        }
    }
}
